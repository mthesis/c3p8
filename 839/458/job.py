import sys



n=1
tim=180
mem=30
gpu=False


if len(sys.argv)>1:
  n=int(sys.argv[1])
if len(sys.argv)>2:
  tim=int(sys.argv[2])
if len(sys.argv)>3:
  mem=int(sys.argv[3])
if len(sys.argv)>4:
  gpu=bool(sys.argv[4])
n=str(n)

while len(n)<2:
  n="0"+n

print("using...")
print("n=",n)
print("tim=",tim)
print("mem=",mem)
print("gpu=",gpu)



q="""#!/usr/bin/env zsh
#SBATCH --mem-per-cpu=##mem##G
#SBATCH --job-name=run##n##
#SBATCH --output=logs/output.%J.txt
#SBATCH --time ##tim##
#Sbatch --account=thes0678
##gpu##




module load cuda/100
module load cudnn/7.4
module load python/3.6.8

cd /home/sk656163/m/c1/##n##/

python3 main.py
./stdana.sh
"""


q=q.replace("##n##",str(n))
q=q.replace("##tim##",str(tim))
q=q.replace("##mem##",str(mem))
if gpu:
  q=q.replace("##gpu##","#SBATCH --gres=gpu:1")
else:
  q=q.replace("##gpu##","")






with open("job.sh","w") as f:
  f.write(q)












