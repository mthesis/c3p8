from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys

from superageta import *
from debugnan import *






subset=""
#subset="_short"+subset

shallshow=False
shallwrite=True



if len(sys.argv)>1:
  shallwrite=bool(int(sys.argv[1]))
if len(sys.argv)>2:
  fid=int(sys.argv[2])
  if fid==1:subset="_short"+subset
  if fid==2:subset="_fairer"+subset
  if fid==3:subset="_fair"+subset

if shallwrite:
  for i in range(10):
    print("##################################################################")
  print("!!WRITING!!")
  for i in range(10):
    print("##################################################################")









f=np.load("..//..//toptagdataref//train"+subset+".npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f
vf=np.load("..//..//toptagdataref//val"+subset+".npz")
vx=vf["xb"][:vn,:gs,:]
vy=vf["y"][:vn]
del vf





y=keras.utils.to_categorical(y,2)
vy=keras.utils.to_categorical(vy,2)






model=getsupervised()


cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=patience),
                   keras.callbacks.TerminateOnNaN()]
if shallwrite:
  cb.append(keras.callbacks.ModelCheckpoint("superb.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
  cb.append(keras.callbacks.ModelCheckpoint("supermodels/weights{epoch:04d}.tf",verbose=0,save_best_only=False,period=1,save_weights_only=True))
  cb.append(keras.callbacks.CSVLogger("history_super.csv"))


# train the autoencoder
model.fit(x,y,
        epochs=epochs,
        batch_size=batch_size,
        #)
        #validation_split=0.1)
        validation_data=(vx, vy),
        verbose=verbose,
        callbacks=cb)




if shallwrite:model.save_weights('super.tf')

#printananan(model)




