#searches for correlations in the last 40 prediction rows
import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")

y=f["y"]
p=f["p"]

bins=20


offset=len(p[0])-40

rows=np.array([p[:,i] for i in range(len(p[0])-40,len(p[0]))])
print(rows)
print(rows.shape)

c=np.corrcoef(rows)
print(c)



fig, ax = plt.subplots(figsize=(10,10))

def disp(x):
  #if x<0:return "-"+str(round(abs(int(10*x))))
  return str(round(int(x*10)))

ax.matshow(c, cmap=plt.cm.Blues)

for i in range(len(c)):
  for j in range(len(c[i])):
    ax.text(i, j, disp(c[i,j]), va='center', ha='center')



plt.savefig("l40correlations.png",format="png")
plt.savefig("l40correlations.pdf",format="pdf")

plt.show()


