import sys



n=1
tim=120
mem=20



if len(sys.argv)>1:
  n=int(sys.argv[1])
if len(sys.argv)>2:
  tim=int(sys.argv[2])
if len(sys.argv)>3:
  mem=int(sys.argv[3])

n=str(n)

while len(n)<2:
  n="0"+n

print("using...")
print("n=",n)
print("tim=",tim)
print("mem=",mem)




q="""#!/usr/bin/env zsh
#SBATCH --mem-per-cpu=##mem##G
#SBATCH --job-name=srun##n##
#SBATCH --output=logs/output.%J.txt
#SBATCH --gres=gpu:1
#SBATCH --time ##tim##





module load cuda/100
module load cudnn/7.4
module load python/3.6.8

cd /home/sk656163/m/b1/##n##/

python3 supermain.py
"""


q=q.replace("##n##",str(n))
q=q.replace("##tim##",str(tim))
q=q.replace("##mem##",str(mem))







with open("superjob.sh","w") as f:
  f.write(q)












