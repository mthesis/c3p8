import numpy as np
import matplotlib.pyplot as plt
import sys

nams2=["flag","log(pt/pt_jet)"]
nams4=["flag","deta","dphi","log(pt/pt_jet)"]
nams6=["flag","deta","dphi","m","log(E/E_jet)","log(pt/pt_jet)"]

nams={2:nams2,4:nams4,6:nams6}

id=0
if len(sys.argv)>1:
  id=int(sys.argv[1])

fil="evalb.npz"
if len(sys.argv)>2:
  fil="bigevalb.npz"

quiet=False
if len(sys.argv)>3:
  quiet=bool(sys.argv[3])


f=np.load(fil)

p=f["p"]
c=f["c"]

npar=int(p.shape[-1])



def pre(q):
  return np.abs(q[:,:,id])
  return np.mean(np.abs(q),axis=-1)

p=pre(p)
c=pre(c)

def spl(q):
  return np.mean(q,axis=0),np.std(q,axis=0)

mp,sp=spl(p)
mc,sc=spl(c)

if quiet:
  print(nams[npar][id])
  print("i","prediction","reality")
  for i in range(len(mp)):
    print(i,mp[i],"+-",sp[i],mc[i],"+-",sc[i])


if quiet:exit()
x=np.arange(len(mp))


print(mp.shape,sc.shape)

plt.title(nams[npar][id])

plt.errorbar(x,mp,yerr=sp,label="prediction",fmt="o",alpha=0.5)
plt.errorbar(x,mc,yerr=sc,label="reality",fmt="o",alpha=0.5)

plt.legend()

plt.savefig("imgs/outputqual"+str(id)+".png",format="png")
plt.savefig("imgs/outputqual"+str(id)+".pdf",format="pdf")

plt.show()




































