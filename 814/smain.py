import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation, Flatten
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *

objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter}



gs=30
n=300000

param=10
free1=5
it1=10
alin1=[-1.0,1.0]
dgs=5



#f=np.load("..//..//data//smalltrain.npz")
files=[]
for i in range(1):
  files.append(np.load("..//..//data//pregen//tinytrain"+str(i)+".npz"))



#print(f.files)
#exit()

if len(files)>1:
  m4s,m5s,nfeats,ys=[],[],[],[]

  for f in files:
    m4s.append(f["m4"])
    m5s.append(f["m5"])
    nfeats.append(f["nfeat"])
    ys.append(f["y"])


  m4=np.concatenate(m4s,axis=0)
  m5=np.concatenate(m5s,axis=0)


  #feat=f["feat"]
  nfeat=np.concatenate(nfeats,axis=0)

  y=np.concatenate(ys)
else:
  f=files[0]
  m4=f["m4"]
  m5=f["m5"]
  nfeat=f["nfeat"]
  y=f["y"]



m4=m4[:n,:gs,:gs]
m5=m5[:n,:gs,:gs]
m0=np.zeros_like(m4)
nfeat=nfeat[:n,:gs,:]



x=np.concatenate((m0,m4,m5,nfeat),axis=-1)

x=x[:n,:gs,:]
y=y[:n]


y=keras.utils.to_categorical(y,2)


print(x.shape,y.shape,nfeat.shape)



#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               



gb=gkeepbuilder(gs=gs,param=param,free=free1,dimension=2)
g=glkeep(graphmax=gs,graphvar=param+free1,keepconst=param,iterations=it1,alinearity=alin1,dimension=2)
gc=gkeepcutter(inn=gs,param=param+free1,out=gs-dgs,dimension=2)


gf=gfeatkeep(gs=gs-dgs,param=param+free1)


#model=Sequential([gb])#,g,gf,Flatten(),Dense(100,activation="relu"),Dense(20,activation="relu"),Dense(2,activation="softmax")])
model=Sequential([gb,g,gc,gf,Flatten(),Dense(100,activation="relu"),Dense(20,activation="relu"),Dense(2,activation="softmax")])
#model=Sequential([gb])#,g,gf,Flatten(),Dense(100,activation="relu"),Dense(20,activation="relu"),Dense(2,activation="softmax")])
model.summary()
print(model.input_shape)
print(model.compute_output_shape(model.input_shape))

model.compile(RMSprop(lr=0.001),loss="mse",metrics=['accuracy'])

#my=model.predict(x)

#print(my[0])
#print(my.shape)
#print(np.transpose(my[0]))



plot_model(model, to_file='model.png')



fit = model.fit(
    x, y,    # training data
    batch_size=20,  # no mini-batches, see next lecture
    epochs=100,       # number of training epochs
    verbose=2,           # verbosity of shell output (0: none, 1: high, 2: low)
    # validation_data=(vx, vy),  # validation data
    validation_split=0.2,  # validation data
    callbacks=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=10),
               keras.callbacks.ModelCheckpoint("modelb.h5", monitor='val_loss', verbose=verbose, save_best_only=True),
               keras.callbacks.ModelCheckpoint("modela.h5", monitor='val_acc', verbose=verbose, save_best_only=True, mode='auto'),

               keras.callbacks.CSVLogger("history.csv")
    ])        # optional list of functions to be called once per epoch



print("reached model quality of ",np.max(fit.history["val_acc"]),np.min(fit.history["val_loss"]))
np.savez_compressed("result",val_acc=np.max(fit.history["val_acc"]),val_loss=np.min(fit.history["val_loss"]))
