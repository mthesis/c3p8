from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys

from ageta import *








subset=""
subset="_short"+subset

shallshow=False
shallwrite=False

if len(sys.argv)>1:
  shallwrite=bool(int(sys.argv[1]))

if shallwrite:
  for i in range(10):
    print("##################################################################")
  print("!!WRITING!!")
  for i in range(10):
    print("##################################################################")







def plot_results(models,
                 data,
                 batch_size=128):
    model_name="imgs"

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean")
    # display a 2D plot of the digit classes in the latent space
    z_mean, _, _ = encoder.predict(x_test,
                                   batch_size=batch_size)
    plt.figure(figsize=(12, 10))
    plt.scatter(z_mean[:, 0], z_mean[:, 1], c=y_test)
    plt.colorbar()
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename+".png",format="png")
    plt.savefig(filename+".pdf",format="pdf")
    if shallshow:plt.show()


f=np.load("..//..//toptagdataref//train"+subset+".npz")
x=f["xb"][:n,:gs,:]
#y=f["y"][:n]
del f
vf=np.load("..//..//toptagdataref//val"+subset+".npz")
vx=vf["xb"][:vn,:gs,:]
vy=vf["y"][:vn]
del vf


vae,encoder,decoder=getvae()
models=(encoder,decoder)

encoder=gettestmodel()

px=encoder.predict(x)
print(px)

nan=False

for su in px:
  print(su.shape)
  print(np.isnan(np.mean(su)))
  nan=nan or np.isnan(np.mean(su))

if nan:
  for i in range(10):print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
  print("got nan")
  for i in range(10):print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

exit()




cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=patience),
                   keras.callbacks.TerminateOnNaN()]
if shallwrite:
  cb.append(keras.callbacks.ModelCheckpoint("modelb.h5", monitor='val_loss', verbose=verbose, save_best_only=True,save_weights_only=True))
  cb.append(keras.callbacks.ModelCheckpoint("models/weights{epoch:04d}.h5",verbose=0,save_best_only=False,period=1,save_weights_only=True))
  cb.append(keras.callbacks.CSVLogger("history.csv"))


# train the autoencoder
vae.fit(x,
        epochs=epochs,
        batch_size=batch_size,
        #)
        #validation_split=0.1)
        validation_data=(vx, None),
        verbose=verbose,
        callbacks=cb)




if shallwrite:vae.save_weights('vae.h5')

plot_results(models,
             (vx,vy),
             batch_size=batch_size)





