import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Input, Dense, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
import sys
import numpy as np
import matplotlib.pyplot as plt
import os

from smult import *

from loaddata import *


base=[1,3,9,27,9,3,1]

def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


def getmodel(q,reg=None,act="relu",mean=1.0,seed=None):
  if not seed is None:np.random.seed(seed)
  if not seed is None:tf.random.set_seed(seed)
  #inn=Input(shape=(28,28,1))
  inn=Input(shape=(q[0],))
  w=inn
  for aq in q[1:]:
    w=Dense(aq,activation=act,use_bias=False,kernel_initializer=keras.initializers.TruncatedNormal(),kernel_regularizer=reg)(w)
  w=smult(n=q[-1])([w])
  m=Model(inn,w,name="oneoff")
  zero=K.ones_like(w)*mean
  loss=mse(w,zero)
  loss=K.mean(loss)
  m.add_loss(loss)
  m.compile(Adam(lr=0.01))
  return m



def trainone(index,n,l):
  cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=20,restore_best_weights=True),keras.callbacks.TerminateOnNaN()]
  if index is None:
    cb.append(keras.callbacks.ModelCheckpoint(f"msPep{n}.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
    cb.append(keras.callbacks.CSVLogger(f"history_sPep{n}.csv"))
  else:
    cb.append(keras.callbacks.ModelCheckpoint(f"multi/{index}/msPep{n}.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
    cb.append(keras.callbacks.CSVLogger(f"history_multi{index}sPep{n}.csv"))

  m=getmodel(l)
  m.summary()
  h=m.fit(t,None,
          epochs=500,
          batch_size=100,
          validation_split=0.25,
          verbose=1,
          callbacks=cb)



if __name__=="__main__":
  index=None
  try:
    if len(sys.argv)>1:index=int(sys.argv[1])
  except:pass

  if index is None:
    f=np.load("bigcode.npz")
  else:
    f=np.load(f"multi/{index}/bigcode.npz")


  t=f["p"]

  _,tadd=loadtrainplus()

  print(t.shape,tadd.shape)
  tadd=np.reshape(tadd,(len(tadd),1))
  t=np.concatenate((t,tadd),axis=1)




  #print(t.shape)
  mult=int(t.shape[-1])
  l=list([mult*ac for ac in base])

  ex=[1,5]
  if len(sys.argv)>2:ex=eval(sys.argv[2])

  for i in range(ex[0],ex[1]+1):
    trainone(index=index,n=i,l=l)
    if len(sys.argv)>2:
      if index is None:
        os.system("python3 sPeval.py zero "+str(i))
      else:
        os.system(f"python3 sPeval.py {index} "+str(i))

    



















