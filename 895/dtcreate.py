import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





class dtcreate(Layer):
  def __init__(self,gs=20,param=40,k=16,flag=0,kernel_initializer='glorot_uniform',numericalC=10000.0,**kwargs):
    self.gs=gs
    self.param=param
    self.k=k
    self.flag=flag
    self.kernel_initializer=kernel_initializer
    self.numericalC=numericalC
    super(dtcreate,self).__init__(**kwargs)

  def build(self, input_shape):


    self.metrik=self.add_weight(name="metrik",
                                shape=(self.param,),
                                initializer=keras.initializers.Ones(),
                                trainable=False)



    self.built=True


  def call(self, point_cloud):
    point_cloud=point_cloud[0]
    def getDistanceMatrix(x):
      """ Compute pairwise distance matrix for a point cloud

      Input:
      point_cloud: tensor (batch_size, n_points, n_features)
      
      Returns:
      dists: tensor (batch_size, n_points, n_points) pairwise distances
      """

      #print("disting on",x.shape)
      part1 = -2 * K.batch_dot(x*self.metrik, K.permute_dimensions(x, (0, 2, 1)))
      #print("part1",part1.shape)
      poww=K.expand_dims(K.sum((x**2)*self.metrik,axis=2)+(1-x[:,:,self.flag])*self.numericalC)
      part2 = K.permute_dimensions(poww, (0, 2, 1))
      #print("part2",part2.shape)
      part3 = poww
      #print("part3",part3.shape)
      dists = part1 + part2 + part3
      #print("dists",dists.shape)
      #exit()
      return dists

    def getKnearest(dists, k):
      """Get indices of k nearest neighbors from distance tensor
      Input:
      dists: (batch_size, n_points, n_points) pairwise distances
      Returns:
      knn_idx: (batch_size, n_points, k) nearest neighbor indices
      """
      _, knn_idx = tf.math.top_k(-dists, k=k)
      return knn_idx

    def getEdgeFeature(point_cloud, nn_idx):
      """Construct the input for the edge convolution
      Input:
      point_cloud: (batch_size, n_points, n_features)
      nn_idx: (batch_size, n_points, n_neighbors)
      Returns:
      edge_features: (batch_size, n_points, k, n_features*2)
      """
      k = nn_idx.get_shape()[-1]

      point_cloud_shape = tf.shape(point_cloud)
      batch_size = point_cloud_shape[0]
      n_points = point_cloud_shape[1]
      n_features = point_cloud_shape[2]

      # Prepare indices to match neighbors in flattened cloud
      idx = K.arange(0, stop=batch_size, step=1) * n_points
      idx = K.reshape(idx, [-1, 1, 1])

      # Flatten cloud and gather neighbors
      flat_cloud = K.reshape(point_cloud, [-1, n_features])
      neighbors = K.gather(flat_cloud, nn_idx+idx)

      # Expand centers to (batch_size, n_points, k, n_features)
      cloud_centers = K.expand_dims(point_cloud, axis=-2)
      cloud_centers = K.tile(cloud_centers, [1, 1, k, 1])

      edge_features = K.concatenate([cloud_centers, neighbors-cloud_centers], 
          axis=-1)
      return edge_features

    def batch_norm(inputs, gamma, beta, dims, ind):
      normed, batch_mean, batch_var = K.normalize_batch_in_training(
                                          x=inputs,
                                          gamma=gamma,
                                          beta=beta,
                                          reduction_axes=dims)
      
      self.add_update([
        K.moving_average_update(self.moving_mean[ind], batch_mean, 0.9),
        K.moving_average_update(self.moving_var[ind], batch_var, 0.9)])

      normed_moving = K.batch_normalization(
                                          x=inputs,
                                          mean=self.moving_mean[ind],
                                          var=self.moving_var[ind],
                                          beta=beta,
                                          gamma=gamma)

      return K.in_train_phase(normed, normed_moving)


    #print("point_cloud",point_cloud.shape)


    #if self.n_ind:
    #  dists = getDistanceMatrix(point_cloud[:, :, self.n_ind])
    #else:
    dists = getDistanceMatrix(point_cloud)

    #print("dists",dists.shape)


    knn_idx = getKnearest(dists, self.k)
    #print("knn_idx",knn_idx.shape)
    edge_features = getEdgeFeature(point_cloud, knn_idx)
    return edge_features
    print("edge_features",edge_features.shape)
    exit()



    ##output = K.conv2d(edge_features, self.kernel, (1, 1), padding='same')
    #print("prebias",output.shape)
    #exit()
    #return output
    ##output = K.bias_add(output, self.bias)
    #output = batch_norm(output, self.gamma[0], self.beta[0], [0, 1, 2], 0)
    ##output = K.relu(output)
    ##output = K.mean(output, axis=-2)
    return output


 
  def compute_output_shape(self,input_shape):

    #print("torbinp",input_shape)
    #exit()
    a=input_shape[0]
    assert len(a)==3
    assert a[1]==self.gs
    assert a[2]==self.param
    return tuple([input_shape[0],self.gs,self.k,2*self.param])#here no new parameters   

  def get_config(self):
    mi={"gs":self.gs,"param":self.param,"k":self.k,"numericalC":self.numericalC}
    th=super(dtcreate,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return dtcreate(**config)































