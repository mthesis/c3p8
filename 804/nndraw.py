import numpy as np
import matplotlib.pyplot as plt
import sys











def subdraw(q,rang=None,cmap=None):
  eta,phi,pt=q[:,1],q[:,2],np.abs(q[:,3])/rang[-1]



  for j in range(len(eta)):
    plt.plot([eta[j]],[phi[j]],"o",alpha=0.5,color=cmap,markersize=1+9/(pt[j]+1))


  #plt.hist2d(x=eta,y=phi,weights=pt*0+1,density=True,range=range,cmap=cmap,alpha=0.5,bins=20)




def draw(i,p,c,l,xlim=None,ylim=None,ot=""):
  plt.close()
  if xlim !=None:plt.xlim(xlim)
  if ylim !=None:plt.ylim(ylim)
  exec(ot)
  plt.title("loss["+str(i)+"]="+str(round(l,4)))

  ptmin=np.min([np.min(np.abs(p[:,3])),np.min(np.abs(c[:,3]))])
  ptmax=np.max([np.max(np.abs(p[:,3])),np.max(np.abs(c[:,3]))])

  range=[ptmin,ptmax]

  subdraw(p,rang=range,cmap="Blue")
  subdraw(c,rang=range,cmap="Red")

  plt.xlabel("eta")
  plt.ylabel("phi")


  #plt.savefig("imgs/reci/"+str(i)+".png",format="png")
  #plt.savefig("imgs/reci/"+str(i)+".pdf",format="pdf")

  #plt.legend()

def odraw(c,xlim=None,ylim=None,ot=""):
  plt.close()
  if xlim !=None:plt.xlim(xlim)
  if ylim !=None:plt.ylim(ylim)
  exec(ot)

  ptmin=np.min(np.abs(c[:,3]))
  ptmax=np.max(np.abs(c[:,3]))

  range=[ptmin,ptmax]

  #subdraw(p,rang=range,cmap="Blue")
  subdraw(c,rang=range,cmap="Blue")

  plt.xlabel("eta")
  plt.ylabel("phi")


  #plt.savefig("imgs/reci/"+str(i)+".png",format="png")
  #plt.savefig("imgs/reci/"+str(i)+".pdf",format="pdf")

  #plt.legend()

