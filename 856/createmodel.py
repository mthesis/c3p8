import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization,Conv2D
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from gpre5 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *
from gaddbias import *
from gssort import *
from winit import *
from glm import *
from glom import *#probably useless
from glim import *
from ggraphstract import *
from gmake1graph import *
from gcomdepoolplus import *
from gcomdex import *
from gcomjpool import *
from gcomgpool import *
from gchooseparam import *
from gcomdepoollg import *
from gcomdiagraph import *
from gcomreopool import *
from gcomparastract import *
from glam import *
from gcomdensemerge import *
from gcompoolmerge import *
from gcomgraphcutter import *
from gcomdensediverge import *
from gcomgraphfromparam import *
from gcomgraphcombinations import *
from gcomgraphand import *
from gcomgraphrepeat import *
from gcomgraphlevel import *
from gcomgraphand2 import *
from gcomparamlevel import *
from gliam import *
from gcomfullyconnected import *
from gcomparamcombinations import *
from gcomgraphfrom2param import *
from gcomextractdiag import *
from gmultiply import *
from dedge import *
from dexpand import  *
from dnarrow import *
from dtorb import *
from dreflag import *
from dtcreate import *
from dtdestroy import *
from glcreate import *
from glacreate import *
from gshuffle import *
from gortho import *
from gperm import *
from gpartinorm import *
from gcutparam import *
from ghealparam import *
from gswitch import *


objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk,"gltk":gltk,"gltknd":gltknd,"gpre1":gpre1,"gpre2":gpre2,"gpre3":gpre3,"gpre4":gpre4,"gpre5":gpre5,"glmlp":glmlp,"gltrivmlp":gltrivmlp,"gadd1":gadd1,"gaddzeros":gaddzeros,"gsym":gsym,"gbrokengrowth":gbrokengrowth,"gpoolgrowth":gpoolgrowth,"gremoveparam":gremoveparam,"gcompool":gcompool,"gcomdepool":gcomdepool,"gvaluation":gvaluation,"ggoparam":ggoparam,"gfromparam":gfromparam,"gaddbias":gaddbias,"gssort":gssort,"glm":glm,"glom":glom,"glim":glim,"ggraphstract":ggraphstract,"gmake1graph":gmake1graph,"gcomdepoolplus":gcomdepoolplus,"gcomdex":gcomdex,"gcomjpool":gcomjpool,"gcomgpool":gcomgpool,"gchooseparam":gchooseparam,"gcomdepoollg":gcomdepoollg,"gcomdiagraph":gcomdiagraph,"gcomreopool":gcomreopool,"gcomparastract":gcomparastract,"glam":glam,"gcomdensemerge":gcomdensemerge,"gcompoolmerge":gcompoolmerge,"gcomgraphcutter":gcomgraphcutter,"gcomdensediverge":gcomdensediverge,"gcomgraphfromparam":gcomgraphfromparam,"gcomgraphcombinations":gcomgraphcombinations,"gcomgraphand":gcomgraphand,"gcomgraphrepeat":gcomgraphrepeat,"gcomgraphlevel":gcomgraphlevel,"gcomgraphand2":gcomgraphand2,"gcomparamlevel":gcomparamlevel,"gliam":gliam,"gcomfullyconnected":gcomfullyconnected,"gcomparamcombinations":gcomparamcombinations,"gcomgraphfrom2param":gcomgraphfrom2param,"gcomextractdiag":gcomextractdiag,"gmultiply":gmultiply,"dedge":dedge,"dexpand":dexpand,"dnarrow":dnarrow,"dtorb":dtorb,"dreflag":dreflag,"dtcreate":dtcreate,"dtdestroy":dtdestroy,"glcreate":glcreate,"glacreate":glacreate,"gshuffle":gshuffle,"gortho":gortho,"gperm":gperm,"gpartinorm":gpartinorm,"gcutparam":gcutparam,"ghealparam":ghealparam,"gswitch":gswitch}

from spec import spec



###global attributes that probably will never be chanced

flag=0
self_interaction=True

cut=0.5
c_const=1000.0
  

###definitions of classes that will be used later down

class state:
  gs=10
  param=10

  def __init__(self,gs,param):
    self.gs=gs
    self.param=param
  def __repr__(self):
    return str(self.gs)+"*"+str(self.param)
class grap:
  A=None
  X=None
  s=None
  def __init__(self,s):
    self.A=None
    self.X=None
    self.s=s
class setting:
  
  def __init__(self,**kwargs):
    pass
    #setattr(self,key,value)






######to still implement

#def graphcompression(data,gs,param0,param1,c=2):
#  feat0=gvaluation(gs=gs,param=param0)([data])
#  feat1=gcompool(gs=gs,param=param0+1,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([feat0])
#  return feat1
#def graphcompressionv1(data,gs,param0,param1,c=2):
#  #feat0=gvaluation(gs=gs,param=param0)([data])
#  feat1=gcompool(gs=gs,param=param0,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([data])
#  return feat1

#alle compression/decompression level über >=2 aus alten create models?
#ein effizienter weg um die initizer zu steuern


#def multidense(data,d,activation="relu",kernel_initializer=keras.initializers.Identity()):#if applied to 2d data(nodes, attributes), works on each node
#  if len(d)==0 or False:return data
#  ac=d[0]
#  d=d[1:]
#
#  lay=Dense(int(ac),activation=activation,kernel_initializer=kernel_initializer)
#  data=lay(data)
#  return multidense(data,d,activation=activation,kernel_initializer=kernel_initializer)



def multidense(g,m,q):
  '''runs just a list of Dense Layers (defined by m.mdense* and by the parameter q, which gives width and number of Layers) on the last axis of the input data'''
  for d in q:
    g.X=Dense(d,activation="linear",kernel_initializer=m.mdense_init_kernel,bias_initializer=m.mdense_init_bias,use_bias=m.mdense_usebias)(g.X)
    if m.mdense_batchnorm:g.X=BatchNormalization()(g.X)
    if m.mdense_activation!="linear":g.X=Activation(m.mdense_activation)(g.X)
    g.s.param=d
  return g


def norm(g,scale=True):
  g.X=BatchNormalization(axis=-1,scale=scale)(g.X)
  return g

def prep(g,m):#preparation handler, return g and input
  inp=Input(shape=(g.s.gs,4))
  feat0=gpre5(gs=g.s.gs)(inp)
  if m.prenorm:norm(g,scale=False)
  g.X=feat0
  g.s.param=4
  return g,inp



def graphatbottleneck(g,shallfp=True):#handles the bottleneck transformations for a pure graph ae, return g, compressed, new input, shallfp=True=>convert vector in matrix (gfromparam)
  comp=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  if m.shallredense:
    for e in m.redenseladder:
      comp=Dense(e,activation=m.redenseactivation,kernel_initializer=m.redenseinit)(comp)
    inn2=Input(m.redenseladder[-1])
    use=inn2
    for i in range(len(m.redenseladder)-1,-1,-1):
      use=Dense(m.redenseladder[i],activation=m.redenseactivation,kernel_initializer=m.redenseinit)(use)
    use=Dense(g.s.gs*g.s.param,activation=m.redenseactivation,kernel_initializer=m.redenseinit)(use)
  else:
    inn2=Input(g.s.gs*g.s.param)
    use=inn2

  if shallfp:
    taef1=gfromparam(gs=g.s.gs,param=g.s.param)([use])
  else:
    taef1=inn2
  g.X=taef1
  g.A=None
  return g,comp,inn2



def denseladder(c,n=3,truestart=False):
  if type(c)==list:
    ret=[]
    for ac in c:
      ret.append(denseladder(ac,n=n,truestart=truestart))
    return ret

  if truestart:n-=1

  #generates a list of Dense sizes going from 1 to c in n steps (excluding 1 and c)
  ret=[]
  if truestart:ret.append(1)
  fact=1.0
  mul=c**(1/(n+1))
  for i in range(n):
    fact*=mul
    ret.append(fact)
  return ret
def divtriv(g,c,shallgp=True,addDense=[[]],activation=None):#trivial graph diverger by a factor of c (does not chance param at all)
  #requiregp=True: require ggoparam at the start
  #addDense: intermediate Dense Layers, sizes between 1 and c useful



  param=g.X
  if shallgp:param=ggoparam(gs=g.s.gs,param=g.s.param)([param])
  for d in addDense[0]:param=Dense(int(d*g.s.gs*g.s.param),activation=activation,kernel_initializer=m.trivial_decompress_init_kernel,bias_initializer=m.trivial_decompress_init_bias)(param)
  g.s.gs*=c[0]#actually not completely the same as magic 29, since there was a param mistake
  param=Dense(g.s.gs*g.s.param,activation,kernel_initializer=m.trivial_decompress_init_kernel,bias_initializer=m.trivial_decompress_init_bias)(param)
  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([param])
  c=c[1:]
  if len(addDense)>0:
    addDense=addDense[1:]
  if len(c)>0:
    return divtriv(g,c,shallgp=False,addDense=addDense)
  return g

def remparam(g,nparam):
  g.X=gremoveparam(gs=g.s.gs,inn=g.s.param,out=nparam)([g.X])
  g.s.param=nparam
  return g


def handlereturn(inn1,raw,com,inn2,decom,shallvae):
  #inn1  :   initial input Variable
  #raw   :   preconverted input Variable, for comparison sake
  #com   :   compressed Variable
  #inn2  :   input for decoder
  #decom :   decompressed decoder Variable
  #shallvae: shall i thread this like a variational auto encoder? hier just a bodge of an solution



  
  if not int(inn1.shape[-1])==4:
    print("returning failed at input1 shape",inn1.shape)
    assert False
  if not int(raw.shape[-1])==int(decom.shape[-1]):
    print("returning failed at comparison shape comparison(1) of",raw.shape,"and",decom.shape)
    assert False
  if len(raw.shape)>2:
    if not int(raw.shape[-2])==int(decom.shape[-2]):
      print("returning failed at comparison shape comparison(2) of",raw.shape,"and",decom.shape)
      assert False
  if not int(com.shape[-1])==int(inn2.shape[-1]):
    print("returning failed at compression shape comparison of",com.shape,"and",inn2.shape)
    assert False

  
  c1=com
  c2=com
  
  if shallvae:
    c1=Dense(int(c1.shape[-1]))(c1)
    c2=Dense(int(c2.shape[-1]))(c2)
    for i in range(100):print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("!!!running variational!!!")
    for i in range(100):print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
  return inn1,raw,c1,c2,[],inn2,decom

def sortparam(g,m):
  g.X=gssort(gs=g.s.gs,param=g.s.param,index=m.sortindex)([g.X])
  g.A=None
  return g 



def shuffleinp(g,seed=None):#shuffles the param in a certain way, but keeps it const
  q=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  
  q=gshuffle(gs=1,param=g.s.param*g.s.gs,seed=seed)([q])

  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([q])

  return g

def orthoinp(g,seed=None):#shuffles the param in a certain way, but keeps it const
  q=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  
  q=gortho(gs=1,param=g.s.param*g.s.gs,seed=seed)([q])

  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([q])

  return g

def perminp(g):#shuffles the param in a better way, but keeps it const
  q=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  
  q=gperm(gs=1,param=g.s.param*g.s.gs)([q])

  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([q])

  return g

def pnorm(g):
  zw,g.X=gcutparam(gs=g.s.gs,param1=1,param2=g.s.param-1)([g.X])
  g.X=gpartinorm(gs=g.s.gs,param=g.s.param-1)([g.X])
  g.X=ghealparam(gs=g.s.gs,param1=1,param2=g.s.param-1)([zw,g.X])
  return g
  
def switch(g):
  g.X=gswitch(gs=g.s.gs,param=g.s.param)([g.X])
  zw=g.s.gs
  g.s.gs=g.s.param
  g.s.param=zw
  return g


def decompress(g,m,c):
  if type(c)!=list:c=[c]
  if m.decompress=="trivial":
    ###trivial magic29 decompression
    g,com,inn2=graphatbottleneck(g,shallfp=False)
    g=divtriv(g,c=c,shallgp=False,addDense=denseladder(c=c,n=m.trivial_ladder_n),activation=m.trivial_decompress_activation)
    return g,m,com,inn2
  if m.decompress=="paramlike":
    ###parameter like decompression
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divpar(g,c=ac,usei=m.usei)
      g=gnl(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="graphlike":
    ###graph like decompression
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divgra(g,c=ac,usei=m.usei)
      g=gnl(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="classic":
    ###classic decompression algorithm, with one learnable constant graph abstraction
    g,com,inn2=graphatbottleneck(g)
    g.A=gcomfullyconnected(gs=g.s.gs,param=g.s.param)([g.X])
    for ac in c:
      g=divcla(g,c=ac)
      g=gnl(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="classiclg":
    ###classic decompression algorithm, with learnable graph actions
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divcla2(g,c=ac)
      g=gll(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="ccll":
    ###copy copy learn learn decompression algorithm
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divccll(g,c=ac)
      g=gll(g,usei=m.usei)
    return g,m,com,inn2


def minusOne(g,m,n=1):

  p=g.s.param
  q1=[p for i in range(n)]
  q2=[p-1 for i in range(n)]

  g= multidense(g,m,q1)
  g= multidense(g,m,q2)

  return g
def plusOne(g,m,n=1):

  p=g.s.param
  q1=[p for i in range(n)]
  q2=[p+1 for i in range(n)]

  g= multidense(g,m,q1)
  g= multidense(g,m,q2)

  return g


def createbothmodels(gs,out,n,shallvae=True,**kwargs):






  #not completely convinced that this actually does what i want (since main.py 0 1, also same spikes in other dataset)
  #missed some stuff (sort, norm), should be fixed

  assert gs==int(np.prod(spec))

  global m
  m=setting()
  m.usei=False
  m.decompress="trivial"

  m.trivial_ladder_n=1
  m.trivial_decompress_activation="linear"
  m.trivial_decompress_init_kernel=t.keras.initializers.TruncatedNormal()
  m.trivial_decompress_init_bias=t.keras.initializers.TruncatedNormal()

  m.sortindex=-1
  m.prenorm=False

  m.graph_init_self=t.keras.initializers.TruncatedNormal()
  m.graph_init_neig=t.keras.initializers.TruncatedNormal()
  m.agraph_init_self=m.graph_init_self
  m.agraph_init_neig=m.graph_init_neig

  m.edges=3#particle net like
  m.edgeactivation="relu"
  m.edgeactivationfinal=m.edgeactivation
  m.edgeusebias=False
  m.edgeconcat=False

  m.gq_activation="relu"
  m.gq_init_kernel=tf.keras.initializers.TruncatedNormal()
  m.gq_init_bias=tf.keras.initializers.Zeros()
  m.gq_usebias=False
  m.gq_batchnorm=False

  m.shallcomplex=not True
  m.complexsteps=3 

  m.gqa_activation=m.gq_activation
  m.gqa_init_kernel=m.gq_init_kernel
  m.gqa_init_bias=m.gq_init_bias
  m.gqa_usebias=m.gq_usebias
  m.gqa_batchnorm=m.gq_batchnorm

  m.shallacomplex=m.shallcomplex
  m.complexasteps=m.complexsteps

  m.shallredense=False
  m.redenseladder=[36,30,25,20,]
  m.redenseactivation="relu"
  m.redenseinit=keras.initializers.Identity()

  m.compression_init=keras.initializers.Identity()

  m.mdense_activation="linear"
  m.mdense_init_kernel=tf.keras.initializers.TruncatedNormal()
  m.mdense_init_bias=tf.keras.initializers.TruncatedNormal()
  m.mdense_usebias=False
  m.mdense_batchnorm=False


  c=6
  addparam=3


  s=state(gs=gs,param=None)
  g=grap(s)

  g,inn=prep(g,m)
  g=sortparam(g,m)
  #g=perminp(g)
  g=pnorm(g)



  #g.X=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  #g.s.param*=g.s.gs
  #g.s.gs=1


  raw=g.X

  #print("before",g.X.shape)

  g=minusOne(g,m)
  g=switch(g)
  g=minusOne(g,m)
  g=switch(g)
  
  #print("after",g.X.shape)


  #g=gq(g,m)

  #g.X=glcreate(gs=g.s.gs,param=g.s.param)([g.A,g.X])
  #g.s.param*=2

  #print("gX",g.X.shape)

  #g.X=Dense(4)(g.X)

  #print("gX",g.X.shape)
  #exit()

  #g=ge(g,k=3,m=m)
  #g=learngraph(g,free=1)

  #g=abstr(g,c=c)


  g,com,inn2=graphatbottleneck(g)


  ###variable decompression
  #g,m,com,inn2=decompress(g,m,c=spec[::-1])

  
  #print("before",g.X.shape)
  g=plusOne(g,m)
  g=switch(g)
  g=plusOne(g,m)
  g=switch(g)
  #print("after",g.X.shape)
  #exit()

  #g=remparam(g,nparam=4)

  #g=sortparam(g,m)

  #g.X=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  #g.s.param*=g.s.gs
  #g.s.gs=1



  decom=g.X


  return handlereturn(inn,raw,com,inn2,decom,shallvae)












