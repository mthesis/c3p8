import numpy as np
import matplotlib.pyplot as plt
import sys

q="vl"

if len(sys.argv)>1:
  q=sys.argv[1]


f=np.load("abe.npz")


l=f[q]
a=f["a"]



plt.plot(l,a,"o")

c=np.corrcoef([l,a])[0,1]
lc=np.corrcoef([np.log(l),np.log(a)])[0,1]

plt.title(str(round(c,4))+"("+str(round(lc,4))+")")



plt.xlabel(q)
plt.ylabel("auc")

plt.savefig("imgs/lossbyauc.png",format="png")
plt.savefig("imgs/lossbyauc.pdf",format="pdf")

plt.show()














