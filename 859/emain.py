import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *



objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk}



gs=3
n=8

param0=10
param1=64
param2=128
param3=256
it1=1
alin1=[-1.0,1.0]
alin1=[0.0,"inf"]


v1=[0.0,0.0]
v2=[1.0,0.0]
v3=[0.0,1.0]
v4=[-1.0,-1.0]
v5=[-1.0,1.0]

vs=[v1,v2,v3,v4,v5]
v=[vs]
gs=len(vs)
v=np.array(v)
print(v.shape)



x=v




gtop=gtopk(gs=gs,param=2,k=2)

model=Sequential([gtop])
#model=Sequential([gb])#,g,gf,Flatten(),Dense(100,activation="relu"),Dense(20,activation="relu"),Dense(2,activation="softmax")])
#model=Sequential([gb,g1a,g1b,g1c,gb2,g2a,g2b,g2c,gb3,g3a,g3b,g3c,gf,gp,Flatten(),Dense(256,activation="relu"),Dropout(0.1),Dense(2,activation="softmax")])
#model=Sequential([gb])#,g,gf,Flatten(),Dense(100,activation="relu"),Dense(20,activation="relu"),Dense(2,activation="softmax")])
model.summary()
print(model.input_shape)
print(model.compute_output_shape(model.input_shape))

model.compile(RMSprop(lr=0.001),loss="mse",metrics=['accuracy'])

my=model.predict(x)

print(my[0])
print(my.shape)
#print(np.transpose(my[0]))

exit()

plot_model(model, to_file='model.png')



fit = model.fit(
    x, y,    # training data
    batch_size=20,  # no mini-batches, see next lecture
    epochs=100,       # number of training epochs
    verbose=2,           # verbosity of shell output (0: none, 1: high, 2: low)
    # validation_data=(vx, vy),  # validation data
    validation_split=0.2,  # validation data
    callbacks=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=10),
               keras.callbacks.ModelCheckpoint("modelb.h5", monitor='val_loss', verbose=verbose, save_best_only=True),
               keras.callbacks.ModelCheckpoint("modela.h5", monitor='val_acc', verbose=verbose, save_best_only=True, mode='auto'),

               keras.callbacks.CSVLogger("history.csv")
    ])        # optional list of functions to be called once per epoch



print("reached model quality of ",np.max(fit.history["val_acc"]),np.min(fit.history["val_loss"]))
np.savez_compressed("result",val_acc=np.max(fit.history["val_acc"]),val_loss=np.min(fit.history["val_loss"]))
