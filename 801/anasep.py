import numpy as np
import matplotlib.pyplot as plt
import sys

idd=0
if len(sys.argv)>1:
  idd=int(sys.argv[1])

f=np.load("sep.npz")


bins=20

x,y,s=f["x"],f["y"],f["s"]

s=s[:,idd]


print(np.mean(s))
print(np.std(s))
print(np.mean(np.abs(s)))


min=np.min(s)
max=np.max(s)

plt.hist(s[y==0],range=(min,max),alpha=0.5,label="0",bins=bins)
plt.hist(s[y==1],range=(min,max),alpha=0.5,label="1",bins=bins)

plt.legend()

plt.show()










