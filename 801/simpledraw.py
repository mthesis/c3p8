import numpy as np
import matplotlib.pyplot as plt
from nndraw import draw,subdraw
import sys

from lossfunc import currentloss

#from ageta import gs


index=-1
try:
  if len(sys.argv)>2:index=int(sys.argv[2])
except:
  from os import listdir
  from os.path import isfile
  from vload import advreadloss
  multis=[f for f in listdir("multi") if not isfile(f)]
  losses=[]
  indices=[]
  for m in multis:
    losses.append(advreadloss("multi/"+m+"/"))
    indices.append(m)
  ind=indices[np.argmin(losses)]
  index=int(ind)

  acl=np.min(losses)
  comp=advreadloss("")
  if comp<acl:index=-1

  print(f"using minloss {index}")


addstr=""
if index>-1:
  addstr=f"multi/{index}/"


f=np.load(addstr+"evalb.npz")
p=f["p"]
c=f["c"]
y=f["y"]

#print(p.shape)
#exit()

l=currentloss(p,c,np,finalise=False)


if len(p.shape)==2:
  gs=int(p.shape[-1]/4)
  p=np.reshape(p,(len(p),gs,4))
  c=np.reshape(c,(len(c),gs,4))



i=0
try:
  if len(sys.argv)>1:i=int(sys.argv[1])
except:
  i=np.argmin(l)
  print(f"using mindex {i}")

print(y[i])

draw(i=i,p=p[i],c=c[i],l=-1)

plt.legend()

plt.savefig("imgs/simpledraw"+str(i)+".png",format="png")
plt.savefig("imgs/simpledraw"+str(i)+".pdf",format="pdf")


plt.show()




