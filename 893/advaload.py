import numpy as np
import csv
import sys

from advload import *
from createmodel import *

def makelenx(q,x):
  q=str(q)
  while len(q)<x:
    q="0"+q
  return q


def aaload(j="b",**kwargs):
  #like adv2load but loads all models
  if j=="a":j="val_acc"
  if j=="b":j="val_loss"

  with open("history.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        q[i].append(float(row[i]))


  row=[]

  for i in range(1,len(q)):
    if qn[i]==j:
      row=q[i]


  ret=[]
  for i in range(len(row)):
    ret.append(aload("models/weights"+makelenx(i+1,4)+".tf"))
  return ret
  

















