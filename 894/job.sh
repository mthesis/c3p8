#!/usr/bin/env zsh
#SBATCH --mem-per-cpu=30G
#SBATCH --job-name=run894
#SBATCH --output=logs/output.%J.txt
#SBATCH --time 1440
#Sbatch --account=thes0678





module load cuda/100
module load cudnn/7.4
module load python/3.6.8

cd /work/sk656163/m/c3p/894/

python3 main.py 1 2
./stdana.sh
