from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys

from ageta import *
from debugnan import *

from callb import *

from trainingon import *

from EarlyStoppingPlus import *

from loaddata import *

subset=trainingon
#subset="_short"+subset

shallshow=False
shallwrite=True



if len(sys.argv)>1:
  shallwrite=bool(int(sys.argv[1]))
if len(sys.argv)>2:
  fid=int(sys.argv[2])
  if fid==1:subset="_short"+subset
  if fid==2:subset="_fairer"+subset
  if fid==3:subset="_fair"+subset

if shallwrite:
  for i in range(10):
    print("##################################################################")
  print("!!WRITING!!")
  for i in range(10):
    print("##################################################################")







def plot_results(models,
                 data,
                 batch_size=128):
    model_name="imgs"

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean")
    # display a 2D plot of the digit classes in the latent space
    z_all = encoder.predict(x_test,
                                   batch_size=batch_size)
    z_mean=z_all[0]
    plt.figure(figsize=(12, 10))
    plt.scatter(z_mean[:, 0], z_mean[:, 1], c=y_test)
    plt.colorbar()
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename+".png",format="png")
    plt.savefig(filename+".pdf",format="pdf")
    if shallshow:plt.show()


x=loadtrain(subset)
vx=loadval(subset)

#f=np.load("..//..//toptagdataref//train"+subset+".npz")
#x=f["xb"][:n,:gs,:]
#y=f["y"][:n]
#del f
#vf=np.load("..//..//toptagdataref//val"+subset+".npz")
#vx=vf["xb"][:vn,:gs,:]
#vy=vf["y"][:vn]
#del vf



vae,encoder,decoder=getvaeq()
models=(encoder,decoder)



#input_shape=vae.input_shape
#print("input_shape",input_shape)

#output_shape=vae.compute_output_shape(input_shape)
#print("output_shape",output_shape)

#exit()



cb=[EarlyStoppingPlus(monitor='val_loss',patience=patience,min_epoch=min_epoch),
                   keras.callbacks.TerminateOnNaN()]
if shallwrite:
  al=LossHistory()
  cb.append(al)
  
  #cb.append(BatchStore(N=100))
if shallwrite:
  cb.append(keras.callbacks.ModelCheckpoint("modelb.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
  cb.append(keras.callbacks.ModelCheckpoint("models/weights{epoch:04d}.tf",verbose=0,save_best_only=False,period=1,save_weights_only=True))
  cb.append(keras.callbacks.CSVLogger("history.csv"))
else:
  cb.append(keras.callbacks.CSVLogger("history_anyway.csv"))


# train the autoencoder
vae.fit(x,
        epochs=epochs,
        batch_size=batch_size,
        #)
        #validation_split=0.1)
        validation_data=(vx, None),
        verbose=verbose,
        callbacks=cb)




if shallwrite:vae.save_weights('vae.tf')

printananan(vae)




