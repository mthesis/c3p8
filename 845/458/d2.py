import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





batch_size=100
epochs=1000
verbose=2
lr=0.001



class d2(Layer):
  def advrelu(self,x,q):
    i1=(type(q[0])==type(""))
    i2=(type(q[1])==type(""))
    if i1:
      if i2:
        return x
      else:
        return q[1]-K.relu(q[1]-x)
    else:
      if i2:
        return K.relu(x-q[0])+q[0]
      else:
        return K.relu(x-q[0])-K.relu(x-q[1])+q[0]
      
  #high number of iterations fail..why?
  def __init__(self,inn,**kwargs):

    self.inn=inn

    self.dense1=keras.layers.Dense(10)
    self.dense2=keras.layers.Dense(10)

    self.dense1.build(input_shape=(inn,))
    self.dense2.build(input_shape=(10,))

    super(d2,self).__init__(**kwargs)

  def get_config(self):
    mi={"inn":self.inn}
    th=super(d2,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return d2(**config)

  
  def build(self, input_shape):
    #self.intact=self.add_weight(name='interaction',
    #                            shape=(self.param,self.param-self.keepconst,),
    #                            initializer=self.kernel_initializer,
    #                            trainable=True)

    
    self.built=True


  def call(self,x):
    return self.dense2.call(self.dense1.call(x))


    
  def compute_output_shape(self,input_shape):
    shape=list(input_shape[0])
    shape[-1]=10
    return tuple(shape)










