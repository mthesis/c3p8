#generates a lot of pictures, displaying the values of one parameter each, once for qcd and once for tops
#here alterations to be able to also display other(y==2) data

import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")
f2=np.load("bcodesv.npz")

y=np.concatenate((f["y"],f2["y"]))
p=np.concatenate((f["p"],f2["p"]))

alpha=0.5
bins=20


offset=0

my=int(np.max(y))

for i in range(offset,len(p[0])):
#q=np.sum(p,axis=-1)
  q=p[:,i]

  plt.title(str(np.mean(q))+","+str(np.std(q))+","+str(np.min(q))+","+str(np.max(q)))

  for k in range(my+1):
    plt.hist(q[y==k],alpha=alpha,bins=bins,label=str(k))
  plt.legend()
  plt.savefig("imgs/multianasv"+makelenx(i,3)+".png",format="png")
  print("did",i,len(p[0]))
  plt.close()

