import numpy as np
import os
from os import path as op
from cauc import caucd

folds=[int(q) for q in os.listdir("multi/") if op.isfile("multi/"+q+"/sepout.npz")]

files=[f"multi/{q}/sepout.npz" for q in folds]
if op.isfile("sepout.npz"):
  files.insert(0,"sepout.npz")
  folds.insert(0,0)

fs=[np.load(q,allow_pickle=True) for q in files]

aucs=[f["auc"] for f in fs]
wids=[f["wid"].tolist()["mean"] for f in fs]
sizs=[len(f["aucs"]) for f in fs]

ds=[f["d"] for f in fs]
y=fs[0]["y"]


print("the indx:",*folds)
print("all aucs:",*aucs)
print("all wids:",*wids)
print("all sizs:",*sizs)



d=np.sum([d*s for d,s in zip(ds,sizs)],axis=0)/np.sum(sizs)
q=caucd(d=d,y=y)
print("together:")
print("auc:",q["auc"])
print("wid:",np.std(d))
print("siz:",np.sum(sizs))



exit()


w=wids[0]

for i in range(10):print("!!!!")

print(w)
print(type(w))
print(w.shape)
print(w.view())
print(type(w.tolist()))
print(w.tolist()["mean"])

