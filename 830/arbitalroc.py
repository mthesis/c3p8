import numpy as np
from os.path import isfile



files=["roc","data/arbite","data/invarbite","data/twoarbite"]
nams=["standart","opt feat","opt particle","opt both"]

aucs,e30s=[],[]

for fil,nam in zip(files,nams):
  fil+=".npz"
  if isfile(fil):
    f=np.load(fil)
    auc=f["auc"]
    e30=f["e30"]
    aucs.append(auc)
    e30s.append(e30)
    print(nam,auc,1/(e30+0.0001))
  else:
    print(nam,"missing")

exit()

print("d1",aucs[1]-aucs[0])
print("d2",aucs[2]-aucs[0])
print("0+d12",aucs[1]-aucs[0]+aucs[2])

print("m1",aucs[1]/aucs[0])
print("m2",aucs[2]/aucs[0])
print("m12",aucs[1]/aucs[0]*aucs[2])

for i in range(1,len(aucs)):
  print(aucs[i]-aucs[0])



