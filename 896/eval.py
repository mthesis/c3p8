import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv2load import *
from ageta import *

import sys
from loaddata import *

x,y=loadeval()

#f=np.load("..//..//toptagdataref//val_fairer.npz")
#x=f["xb"][:n,:gs,:]
#y=f["y"][:n]
#del f

#y=keras.utils.to_categorical(y,2)

index=None
if len(sys.argv)>1:index=int(sys.argv[1])


#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

mis=["b"]#,"a"]

#if len(sys.argv)>1:
#  cut=int(sys.argv[1])



#  mis=[["b","a"][cut]]

for mi in mis:
  model=a2load(mi,gs=gs,n=n,index=index)
  modelc=getcomp(index=index)


  model.summary()





  my=model.predict(x,verbose=1)
  mc=modelc.predict(x,verbose=1)
  if type(index)==type(None):
    np.savez_compressed("eval"+mi,x=x,y=y,p=my,c=mc)
  else:
    np.savez_compressed("multi/"+str(index)+"/eval"+mi,x=x,y=y,p=my,c=mc)



