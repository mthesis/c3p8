from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os

from createmodel import *

from spec import spec

from lossfunc import currentloss



gs=int(np.prod(spec))*2
n=600000#here needed to keep the number of samples const
vn=200000#here not really, but why not
out=2
epochs=2000*5
verbose=1
patience=100
min_epoch=500
batch_size=100
learning_rate=0.003

usemse=True
usevae=False
usevar=False
usept=False
uselinear=False
useadvpt=False
usecon=False

useext=True

c_const=0*0.3#defines how important the variational loss is, compared to the usual one, smaller than one means, it is less important
varconst=0.01#defines how important the varianz loss is
varignore=1
varbase=0.5
linearm=1
linearb=6
ptfunc=lambda x:1/(0.1+x)**2
weicon=[1.0,1.0,1.0,1.0,1.0,1.0]

def gettestmodel():
  return createtestmodel(gs=gs,n=n,out=out)


# reparameterization trick
# instead of sampling from Q(z|X), sample epsilon = N(0,I)
# z = z_mean + sqrt(var) * epsilon
def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.

    # Arguments
        args (tensor): mean and log of variance of Q(z|X)

    # Returns
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args

    ##disable variations
    #return z_mean+z_log_var

    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon

def getvaeq():
  if usevae:
    return getvae()
  else:
    return getnvae()
def getvae(shallvae=True):
  inputs,comp,z_mean,z_log_var,mats,inputs2,outputs=createbothmodels(gs=gs,n=n,out=out,shallvae=shallvae)



  #print("pregen")
  #exit()


  # use reparameterization trick to push the sampling out as input
  # note that "output_shape" isn't necessary with the TensorFlow backend
  if shallvae:
    z = Lambda(sampling, output_shape=(out,), name='z')([z_mean, z_log_var])
  else:
    z=z_mean

  encout=[z_mean,z_log_var,z]
  encout+=mats
  # instantiate encoder model
  encoder = Model(inputs, encout, name='encoder')
  encoder.summary()
  plot_model(encoder, to_file='encoder.png', show_shapes=True)

  # build decoder model
  decoder = Model(inputs2,outputs,name='decoder')
  #decoder=createantimodel(gs=gs,n=n,out=out)
  decoder.summary()
  plot_model(decoder, to_file='decoder.png', show_shapes=True)


  #return 0,encoder,decoder

  outputs=decoder(encoder(inputs)[2:])#overwrites old outputs, not the most elegant way
  # instantiate VAE model
  #outputs = decoder(encoder(inputs)[2])
  vae = Model(inputs, outputs, name='vae')

  models = (encoder, decoder)


  #print(comp.shape,outputs.shape)
  #exit()


  # VAE loss = mse_loss or xent_loss + kl_loss
  if usemse:


    if useext:
      
      reconstruction_loss=currentloss(comp,outputs,K)

    elif useadvpt:

      #print("comp",comp.shape)
      #print("outputs",outputs.shape)


      xcomp=K.reshape(comp,(-1,4))
      xout=K.reshape(outputs,(-1,4))
      #exit()

      xloss=K.mean((xcomp-xout)**2,axis=-1)
      xweig=ptfunc(xcomp[:,-1])
      
      reconstruction_loss=xloss*xweig


    else:
      quick=False
      if usept:
        weigths=ptfunc(K.mean(K.abs(comp[:,:,-1]),axis=0))
      elif uselinear:
        weigths=K.arange(gs)
        weigths=1/(linearb+linearm*weigths)
      elif usecon:
        weigths=weicon
      else:
        reconstruction_loss = mse(comp, outputs)
        quick=True
      if not quick:
        weigths=K.reshape(weigths,(gs,1))


        featloss=K.mean((comp-outputs)**2,axis=-1)
        featloss=K.dot(featloss,weigths)
        reconstruction_loss=featloss
  else:
    reconstruction_loss = binary_crossentropy(comp,outputs)
  if len(reconstruction_loss.shape)>0:reconstruction_loss=K.mean(reconstruction_loss,axis=-1)

  #print("rec_loss",reconstruction_loss.shape)

  #reconstruction_loss *= 4*gs#original_dim

  #print("rec_loss2",reconstruction_loss.shape)

  kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
  kl_loss = K.mean(kl_loss, axis=-1)
  #kl_loss = K.sum(kl_loss, axis=-1)
  kl_loss *= -0.5

  #print("k1_loss",kl_loss.shape)
  #exit()


  #variance=K.std(outputs,axis=0)
  if usevar:
    variance=K.max(outputs,axis=0)-K.min(outputs,axis=0)
    varloss=1/(varbase+variance)
    varloss=varloss[:,varignore:]
    varloss=K.mean(varloss)
  



  vae_loss = K.mean(reconstruction_loss + c_const*kl_loss)
  if usevar:vae_loss+=varconst*varloss
  #vae.add_loss(vae_loss)
  vae.add_loss(vae_loss)
  vae.compile(Adam(lr=learning_rate))
  vae.summary()
  plot_model(vae,
             to_file='vae.png',
             show_shapes=True)
  return vae,encoder,decoder





def getnvae():
  return getvae(shallvae=False)










