import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd




f=np.load("nd.npz")

d=f["d"]
y=f["y"]



q=caucd(d=d,y=y)

auc=q["auc"]
e30=q["e30"]

print("auc",auc,"e30",e30)


