import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





class dexpand(Layer):
  def __init__(self,gs=20,param=40,**kwargs):
    self.gs=gs
    self.param=param
    super(dexpand,self).__init__(**kwargs)

  def build(self, input_shape):

    self.built=True

  def call(self,x):
    a=x[0]
   
    return K.reshape(a,(-1,self.gs,1,self.param))

 
  def compute_output_shape(self,input_shape):
    a=input_shape[0]
    assert len(a)==3
    assert a[1]==self.gs
    assert a[2]==self.param
    return tuple([input_shape[0],self.gs,1,self.param])#here no new parameters   

  def get_config(self):
    mi={"gs":self.gs,"param":self.param}
    th=super(dexpand,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return dexpand(**config)































