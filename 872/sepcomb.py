import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd


f1=np.load("sepout.npz")
f2=np.load("sepLout.npz")

d1=f1["d"]
d2=f2["d"]
y=f1["y"]


def getauc(d,y):
  return caucd(d=d,y=y)["auc"]

a1=getauc(d1,y)
a2=getauc(d2,y)


print(a1,a2)

def forc(c):
  print("working on c",c)
  d=d1+c*d2
  return getauc(d=d,y=y)

cs=np.arange(-7,-2,0.1)
cs=np.exp(cs)

aucs=[forc(c) for c in cs]


plt.plot(cs,aucs)

plt.show()





