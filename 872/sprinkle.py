import numpy as np


def relu(x):
  return (x+np.abs(-x))/2

def heavi(x):
  C=10000
  return relu(C*x)-relu(C*x-1)



def sprinkle(x,gs):
  """addon function for additional parameters for sepP, needs to return an array of (same lengsth as x, ?)"""

  cou=np.sum(heavi(np.sum(np.abs(x),axis=-1)),axis=-1)

  lc=len(cou)
 
  cou=np.reshape(cou,(lc,1))

  radii=np.sqrt(x[:,:,1]**2+x[:,:,2]**2)#[:,:gs]

  maxrad=np.reshape(np.max(radii,axis=-1),(lc,1))
  meanrad=np.reshape(np.mean(radii,axis=-1),(lc,1))

  return np.concatenate((cou,maxrad,meanrad),axis=1)
  




