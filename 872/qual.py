import numpy as np
from sklearn.metrics import roc_curve
from sklearn.metrics import auc as cauc
#from sklearn.metrics import accuracy_score as cacc
import matplotlib.pyplot as plt

import sys

sshow=False
if len(sys.argv)>1:
  sshow=int(sys.argv[1])>0

def refit(q):#since roc_curve can only do binary...this can only do binary
  return q[:,1]

  ret=[]
  for e in q:
    ret.append(e[1])
  return ret

def cacc(q,p):
  same=0
  for i in range(len(q)):
    if (q[i]>0.5)==(p[i]>0.5):
      same+=1
  return same/len(q)



for mi in ["b"]:
  f=np.load("eval"+mi+".npz")
  
  y=refit(f["y"])
  my=refit(f["my"])

 
  fpr, tpr, thresholds = roc_curve(y,my)
  #print(fpr)
  #print(tpr)
  #print(thresholds)
  
  auc=cauc(fpr,tpr)
  acc=cacc(y,my) 
  
  print(mi,"got",auc,acc)
  
  np.savez_compressed("out"+mi,fpr=fpr,tpr=tpr,auc=auc,acc=acc)
  
  
  
  plt.plot(fpr,tpr,label="model"+mi+" auc:"+str(round(auc,4))+" acc:"+str(round(acc,4)))
  plt.legend()
  
  
plt.savefig("roc.png",format="png")
plt.savefig("roc.pdf",format="pdf")
  
if sshow:plt.show()
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
