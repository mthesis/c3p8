import numpy as np
import matplotlib.pyplot as plt
import sys


slic=1
if len(sys.argv)>1:
  slic=int(sys.argv[1])

func=np.max
if len(sys.argv)>2:
  fu=sys.argv[2]
  if fu=="mean":func=np.mean
  if fu=="min":func=np.min

log=True
if len(sys.argv)>3:
  log=bool(sys.argv[3])

f=np.load("batchloss.npz")

l=f["l"]
e=f["e"]
b=f["b"]



maxb=np.max(b)
index=e+(b/maxb)


l=l[:-len(l)%slic+len(l)]
index=index[:-len(index)%slic+len(index)]

#print(np.array(l).shape)
l=np.reshape(l,(int(len(l)/slic),slic))

#print(np.array(l).shape)

l=np.max(l,axis=-1)

#print(np.array(l).shape)

#print(l)
#print(np.array(l).shape)
#exit()



#l=l[::slic]



index=index[::slic]


plt.plot(index,l)

plt.xlabel("epoch")
plt.ylabel("loss")


if log:plt.yscale("log",nonposy="clip")

plt.savefig("imgs/batchhist.png",format="png")
plt.savefig("imgs/batchhist.pdf",format="pdf")


plt.show()





