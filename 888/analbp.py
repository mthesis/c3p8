import numpy as np
import matplotlib.pyplot as plt
import sys

nams2=["flag","log(pt/pt_jet)"]
nams4=["flag","deta","dphi","log(pt/pt_jet)"]
nams6=["flag","deta","dphi","m","log(E/E_jet)","log(pt/pt_jet)"]

nams={2:nams2,4:nams4,6:nams6}

id=0
if len(sys.argv)>1:
  id=int(sys.argv[1])

fil="evalb.npz"
if len(sys.argv)>2:
  fil="bigevalb.npz"


f=np.load(fil)

p=f["p"]
c=f["c"]

def pre(q):
  return np.abs(q[:,:,id])
  return np.mean(np.abs(q),axis=-1)

p=pre(p)
c=pre(c)


delt=np.sqrt(np.mean((p-c)**2,axis=0))


x=np.arange(len(delt))



plt.title(nams[p.shape[1]][id])

plt.plot(x,delt,"o",label="loss")

plt.legend()

plt.savefig("imgs/outputloss"+str(id)+".png",format="png")
plt.savefig("imgs/outputloss"+str(id)+".pdf",format="pdf")

plt.show()




































