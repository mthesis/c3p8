import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv3load import *
from ageta import *

import sys

job="quark"
if len(sys.argv)>1:job=sys.argv[1]


f=np.load("..//..//rdata//"+job+".npz")
x=f["q"][:n,:gs,:]
del f

index=None
if len(sys.argv)>2:
  index=int(sys.argv[2])



#y=keras.utils.to_categorical(y,2)




#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

model=loadencoder(gs=gs,n=n,index=index)
#modelc=getcomp()


model.summary()





my=model.predict(x,verbose=1)

my=my[0]

print(my)
print(len(my),len(my[0]))

#mc=modelc.predict(x,verbose=1)
if index is None:
  np.savez_compressed("rbcode_"+job,x=x,p=my)
else:
  np.savez_compressed(f"multi/{index}/rbcode_"+job,x=x,p=my)




