import numpy as np

def adaptmean(q,O):
  if len(q.shape)>=2:
    return O.mean(q,axis=(-1,-2))
  if len(q.shape)==1:
    return O.mean(q)
  return q

def powerloss(a,b,O,pwr=2):
  d=O.abs(a-b)**pwr
  return adaptmean(d,O)**(1/pwr)
  return O.mean(d,axis=(-1,-2))**(1/pwr)

def rootplusloss(a,b,O,alpha=1.0):
  d=O.sqrt(alpha+O.abs(a-b))
  return (adaptmean(d,O)-O.sqrt(alpha))**2
  return (O.mean(d,axis=(-1,-2))-O.sqrt(alpha))**2

def relu(x,O):
  if O==np:
    return x*(x>0)
  else:
    return O.relu(x)

def steppingstone(x,a,O,C=10000):
  return (O.exp(-2*O.abs(x))+0.1)/1.1
  #return (relu(C*(x-a)+1,O)-relu(C*(x-a),O))
  return 1-(relu(C*(x-a)+1,O)-relu(C*(x-a),O))

def minima(a,b,O):
  aa=O.reshape(a,[-1,*a.shape[1:],1])
  bb=O.reshape(b,[-1,*b.shape[1:],1])
  #print(aa.shape,bb.shape)
  cc=O.concatenate((aa,bb),axis=-1)
  #print(cc.shape)
  #exit()
  return O.min(cc,axis=-1)

def ptstoneloss(a,b,O,R=0.1):

  odex=len(b.shape)

  if odex==0:return powerloss(a,b,O,pwr=2)

  if odex==3:dang=O.sqrt((a[:,:,1]-b[:,:,1])**2+(a[:,:,2]-b[:,:,2])**2)
  if odex==2:dang=O.sqrt((a[:,1]-b[:,1])**2+(a[:,2]-b[:,2])**2)
  if odex==1:dang=O.sqrt((a[1]-b[1])**2+(a[2]-b[2])**2)
  #if odex==1:dang=O.sqrt((a[1]-b[1])**2+(a[2]-b[2])**2)
  stone=steppingstone(dang,a=R,O=O)

  #if np.sum(stone)>0:print(f"stone {np.sum(stone)}")
  #print("mang",np.min(dang))

  if odex==3:pta=a[:,:,-1]
  if odex==2:pta=a[:,-1]
  if odex==1:pta=a[-1]

  #pta=O.min(pta,axis=-1)-pta

  if odex==3:ptb=b[:,:,-1]
  if odex==2:ptb=b[:,-1]
  if odex==1:ptb=b[-1]

  #ptb=minima(pta,ptb,O)*stone


  dpt=(pta-ptb)**2

  dpt=dpt*stone+(1-stone)



  dpt=O.mean(dpt,axis=-1)

  return dpt# + O.mean((a-b)**2,axis=(-1,-2))*0.25 



def currentloss(a,b,O,finalise=True):
  """calculcates the difference between a and b using O, assumes shape to be 3d, does not need the first dimension to be specified, obviously a and b need to have the same shape"""

  #print(a.shape,b.shape)
  #exit()

  
  q=powerloss(a,b,O,pwr=1)

  #q=ptstoneloss(a,b,O,R=0.1)

  if finalise:
    return O.mean(q)
  else:
    return q



