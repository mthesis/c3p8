import numpy as np
import matplotlib.pyplot as plt
import os
import json

from cauc import caucd
from trainingon import *

import sys

job="quark"
if len(sys.argv)>1:
  job=sys.argv[1]


index=None
if len(sys.argv)>2:
  index=int(sys.argv[2])


def caucdp(d,y):
  ret=caucd(d=d,y=y)
  if ret["auc"]<0.5:
    rel=caucd(d=d,y=1-y)
    ret["e30"]=rel["e30"]
    ret["i30"]=rel["i30"]
  return ret

def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


basestr="rseval_"+job+"_"
altstr="seval"

if index is None:
  fnams=list([q for q in os.listdir(".") if basestr+"1" in q and ".npz" in q])
else:
  fnams=list([f"multi/{index}/"+q for q in os.listdir(f"multi/{index}/") if basestr in q and ".npz" in q])

def trainonwhich(trainingon):
  if "1" in trainingon:return 1
  if "0" in trainingon:return 0
  return -1

t=trainonwhich(trainingon)

def deltamean(s,y,t):
  d=np.mean(s,axis=-1)
  if t<0.5:
    dm=np.mean(d[np.where(y<0.5)])
  else:
    dm=np.mean(d[np.where(y>0.5)])
  ret=np.abs(d-dm)
  ret/=np.std(ret)
  return ret

fs=list([np.load(q) for q in fnams])
falts=[np.load(q.replace(basestr,altstr)) for q in fnams]


ss=list([np.mean(f["s"],axis=-1) for f in fs])
y=falts[0]["y"]
ialts=[np.mean(f["s"],axis=-1) for f in fs]
if t<0.5:
  ialts=[ac[np.where(y<0.5)] for ac in ialts]
else:
  ialts=[ac[np.where(y>0.5)] for ac in ialts]
means=[np.mean(ac) for ac in ialts]
#means=[np.mean(ac) for ac in ss]
ds=[np.abs(s-m) for s,m in zip(ss,means)]


np.savez_compressed("rd"+job,ds=ds)

exit()

#ds=list([deltamean(s,y,t) for s in ss if np.std(s)>0.0001])


qs=list([caucdp(d=d,y=y) for d in ds])

aucs=list([q["auc"] for q in qs])
e30s=list([q["e30"] for q in qs])
wids=[np.mean(d) for d in ds]

print("simple aucs:",*aucs)
print("simple e30s:",*e30s)
print("simple wids:",*wids)

sumd=np.mean(np.array(ds),axis=0)
sq=caucdp(d=sumd,y=y)
sauc=sq["auc"]
se30=sq["e30"]
swid=statinf(sumd[np.where(y==t)])



print("sum auc:",sauc)
print("sum e30:",se30)
print("sum wid:",swid)


print("correlation")
if len(qs)<10:
  print(np.corrcoef(ds))
else:
  print(json.dumps(statinf(np.corrcoef(ds)),indent=2))

if index is None:
  np.savez_compressed("sepout",auc=sauc,e30=se30,n=len(qs),aucs=aucs,e30s=e30s,d=sumd,y=y,wid=swid,ds=ds)
else:
  np.savez_compressed(f"multi/{index}/sepout",auc=sauc,e30=se30,n=len(qs),aucs=aucs,e30s=e30s,d=sumd,y=y,wid=swid)



