import numpy as np
from cauc import cauc
from ageta import *

def sortby(q,i):
  return np.array([x for _,x in sorted(zip(i,q))])


c=np.load("data/uc.npz")["c"]
s=np.mean(c,axis=0)

s=(s-np.mean(s))/np.std(s)
s=s.flatten()
i=np.arange(len(s))


i=sortby(q=i,i=s)
s.sort()


f=np.load("evalb.npz")
y=f["y"]
p=f["p"]
c=f["c"]
del f

p=np.reshape(p,(p.shape[0],p.shape[1]*p.shape[2]))
c=np.reshape(c,(c.shape[0],c.shape[1]*c.shape[2]))



ids=[]
aucs=[]
for ai in i:
  ids.append(ai)
  ap=p[:,ids]
  ac=c[:,ids]

  if False:
    mult=1/s[:len(ids)]
    ap*=mult
    ac*=mult



  ca=cauc(p=ap,c=ac,y=y)
  aucs.append(ca["auc"])
  print(ac.shape,aucs[-1])

print(aucs)


plt.plot(aucs)


print("maxauc",np.max(aucs))


plt.title("auc by index")
plt.xlabel("index")
plt.ylabel("auc")

plt.savefig("imgs/aui.png",format="png")
plt.savefig("imgs/aui.pdf",format="pdf")


plt.show()






