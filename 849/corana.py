import numpy as np
import matplotlib.pyplot as plt


f=np.load("bigevalb.npz")
#f=np.load("evalb.npz")

p=f["p"]
c=f["c"]

for i in range(1,4):
  ap=p[:,:,i]
  ac=c[:,:,i]
  cors=[]
  for j in range(len(p)):
    #print(ap[j],ac[j])
    if np.std(ap[j])<0.001 or np.std(ac[j])<0.001:
      #print(f"jumpin{j}")
      continue
    cc=np.corrcoef(ap[j],ac[j])[1,0]
    cors.append(cc)
    if not j%100:print(j)
  plt.close()
  print(f"{i} has mean correlation of {np.mean(cors)} +- {np.std(cors)}")
  plt.title(f"{i} has mean correlation of {np.mean(cors):.6} +- {np.std(cors):.6}")
  plt.hist(cors,bins=200,range=(-1,1))
  plt.savefig(f"imgs/corana{i}.png",format="png")
  plt.savefig(f"imgs/corana{i}.pdf",format="pdf")

  plt.show()


