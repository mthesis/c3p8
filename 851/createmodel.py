import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization,Conv2D
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from gpre5 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *
from gaddbias import *
from gssort import *
from winit import *
from glm import *
from glom import *#probably useless
from glim import *
from ggraphstract import *
from gmake1graph import *
from gcomdepoolplus import *
from gcomdex import *
from gcomjpool import *
from gcomgpool import *
from gchooseparam import *
from gcomdepoollg import *
from gcomdiagraph import *
from gcomreopool import *
from gcomparastract import *
from glam import *
from gcomdensemerge import *
from gcompoolmerge import *
from gcomgraphcutter import *
from gcomdensediverge import *
from gcomgraphfromparam import *
from gcomgraphcombinations import *
from gcomgraphand import *
from gcomgraphrepeat import *
from gcomgraphlevel import *
from gcomgraphand2 import *
from gcomparamlevel import *
from gliam import *
from gcomfullyconnected import *
from gcomparamcombinations import *
from gcomgraphfrom2param import *
from gcomextractdiag import *
from gmultiply import *
from dedge import *
from dexpand import  *
from dnarrow import *
from dtorb import *
from dreflag import *
from dtcreate import *
from dtdestroy import *
from glcreate import *
from glacreate import *
from gshuffle import *
from gortho import *
from gperm import *
from gpartinorm import *
from gcutparam import *
from ghealparam import *

objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk,"gltk":gltk,"gltknd":gltknd,"gpre1":gpre1,"gpre2":gpre2,"gpre3":gpre3,"gpre4":gpre4,"gpre5":gpre5,"glmlp":glmlp,"gltrivmlp":gltrivmlp,"gadd1":gadd1,"gaddzeros":gaddzeros,"gsym":gsym,"gbrokengrowth":gbrokengrowth,"gpoolgrowth":gpoolgrowth,"gremoveparam":gremoveparam,"gcompool":gcompool,"gcomdepool":gcomdepool,"gvaluation":gvaluation,"ggoparam":ggoparam,"gfromparam":gfromparam,"gaddbias":gaddbias,"gssort":gssort,"glm":glm,"glom":glom,"glim":glim,"ggraphstract":ggraphstract,"gmake1graph":gmake1graph,"gcomdepoolplus":gcomdepoolplus,"gcomdex":gcomdex,"gcomjpool":gcomjpool,"gcomgpool":gcomgpool,"gchooseparam":gchooseparam,"gcomdepoollg":gcomdepoollg,"gcomdiagraph":gcomdiagraph,"gcomreopool":gcomreopool,"gcomparastract":gcomparastract,"glam":glam,"gcomdensemerge":gcomdensemerge,"gcompoolmerge":gcompoolmerge,"gcomgraphcutter":gcomgraphcutter,"gcomdensediverge":gcomdensediverge,"gcomgraphfromparam":gcomgraphfromparam,"gcomgraphcombinations":gcomgraphcombinations,"gcomgraphand":gcomgraphand,"gcomgraphrepeat":gcomgraphrepeat,"gcomgraphlevel":gcomgraphlevel,"gcomgraphand2":gcomgraphand2,"gcomparamlevel":gcomparamlevel,"gliam":gliam,"gcomfullyconnected":gcomfullyconnected,"gcomparamcombinations":gcomparamcombinations,"gcomgraphfrom2param":gcomgraphfrom2param,"gcomextractdiag":gcomextractdiag,"gmultiply":gmultiply,"dedge":dedge,"dexpand":dexpand,"dnarrow":dnarrow,"dtorb":dtorb,"dreflag":dreflag,"dtcreate":dtcreate,"dtdestroy":dtdestroy,"glcreate":glcreate,"glacreate":glacreate,"gshuffle":gshuffle,"gortho":gortho,"gperm":gperm,"gpartinorm":gpartinorm,"gcutparam":gcutparam,"ghealparam":ghealparam}

from spec import spec



###global attributes that probably will never be chanced

flag=0
self_interaction=True

cut=0.5
c_const=1000.0
  

###definitions of classes that will be used later down

class state:
  gs=10
  param=10

  def __init__(self,gs,param):
    self.gs=gs
    self.param=param
  def __repr__(self):
    return str(self.gs)+"*"+str(self.param)
class grap:
  A=None
  X=None
  s=None
  def __init__(self,s):
    self.A=None
    self.X=None
    self.s=s
class setting:
  
  def __init__(self,**kwargs):
    pass
    #setattr(self,key,value)






######to still implement

#def graphcompression(data,gs,param0,param1,c=2):
#  feat0=gvaluation(gs=gs,param=param0)([data])
#  feat1=gcompool(gs=gs,param=param0+1,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([feat0])
#  return feat1
#def graphcompressionv1(data,gs,param0,param1,c=2):
#  #feat0=gvaluation(gs=gs,param=param0)([data])
#  feat1=gcompool(gs=gs,param=param0,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([data])
#  return feat1

#alle compression/decompression level über >=2 aus alten create models?
#ein effizienter weg um die initizer zu steuern


#def multidense(data,d,activation="relu",kernel_initializer=keras.initializers.Identity()):#if applied to 2d data(nodes, attributes), works on each node
#  if len(d)==0 or False:return data
#  ac=d[0]
#  d=d[1:]
#
#  lay=Dense(int(ac),activation=activation,kernel_initializer=kernel_initializer)
#  data=lay(data)
#  return multidense(data,d,activation=activation,kernel_initializer=kernel_initializer)



def norm(g,scale=True):
  g.X=BatchNormalization(axis=-1,scale=scale)(g.X)
  return g

def prep(g,m):#preparation handler, return g and input
  inp=Input(shape=(g.s.gs,4))
  feat0=gpre5(gs=g.s.gs)(inp)
  if m.prenorm:norm(g,scale=False)
  g.X=feat0
  g.s.param=4
  return g,inp

def gq(g,m,steps=4):
  shape0=g.X.shape
  g.X=glcreate(gs=g.s.gs,param=g.s.param)([g.A,g.X])
  for d in denseladder(c=2,n=steps,truestart=True)[::-1]:
    g.X=Dense(int(d*g.s.param),activation="linear",kernel_initializer=m.gq_init_kernel,bias_initializer=m.gq_init_bias,use_bias=m.gq_usebias)(g.X)
    if m.gq_batchnorm:g.X=BatchNormalization()(g.X)
    if m.gq_activation!="linear":g.X=Activation(m.gq_activation)(g.X)
  return g
def gaq(g,m,a,steps=4):
  shape0=g.X.shape
  g.X=glacreate(gs=g.s.gs,a=a,param=g.s.param)([g.A,g.X])
  for d in denseladder(c=2,n=steps,truestart=True)[::-1]:
    g.X=Dense(int(d*g.s.param),activation="linear",kernel_initializer=m.gqa_init_kernel,bias_initializer=m.gqa_init_bias,use_bias=m.gqa_usebias)(g.X)
    if m.gqa_batchnorm:g.X=BatchNormalization()(g.X)
    if m.gqa_activation!="linear":g.X=Activation(m.gqa_activation)(g.X)
  return g

def gnl(g,alin=[],iterations=1,repeat=1,usei=False):#simple graph layer system, without learnable graphs
  if m.shallcomplex:
    return gq(g,m,steps=m.complexsteps)
  if usei:
    g.X=glim(gs=g.s.gs,param=g.s.param,iterations=iterations,alinearity=alin,self_initializer=m.graph_init_self,neig_initializer=m.graph_init_neig)([g.A,g.X])
  else:
    g.X=glm(gs=g.s.gs,param=g.s.param,iterations=iterations,alinearity=alin,self_initializer=m.graph_init_self,neig_initializer=m.graph_init_self)([g.A,g.X])
  if repeat>1:return gnl(g,alin=alin,iterations=iterations,repeat=repeat-1,usei=usei)
  return g
def learngraph(g,free=0,k=4):
  mat,feat=gtopk(gs=g.s.gs,param=g.s.param,free=free,flag=flag,self_interaction=self_interaction,k=k)([g.X])
  g.A=mat
  g.s.param+=free
  g.X=feat
  return g
def gll(g,free=0,alin=[],iterations=1,repeat=1,subrepeat=1,usei=False,k=4):#simple graph layer system, with learnable graphs
  g=learngraph(g,free=free,k=k)
  g=gnl(g,alin=alin,iterations=iterations,repeat=subrepeat,usei=usei)
  if repeat>1:return gll(g,free=free,alin=alin,iterations=iterations,repeat=repeat-1,usei=usei)
  return g

def ganl(A,X,gs,a,param,iterations=1,alin=[],usei=False):
  if not m.shallacomplex:
    if usei:
      return gliam(gs=gs,a=a,param=param,iterations=iterations,alinearity=alin,self_initializer=m.agraph_init_self,neig_initializer=m.agraph_init_neig)([A,X])
    else:
      return glam(gs=gs,a=a,param=param,iterations=iterations,alinearity=alin,self_initializer=m.agraph_init_self,neig_initializer=m.agraph_init_neig)([A,X])
  else:
    ag=grap(state(gs=gs,param=param))
    ag.X=X
    ag.A=A
    ag=gaq(ag,m,a=a,steps=m.complexasteps)
    return ag.X
def abstr(g,c,alin=[],iterations=1,repeat=1,multiglam=1,pmode="max",gmode="mean"):#uses (multiglam) glam to abstract a graph into a factor c smaller graph
  #uses pooling to go from c size subgraphs to 1 size dots
  #does not chance param at all
  #uses (pmode) param pooling mode
  #uses (gmode) graph pooling mode
  graph,feat1=gcomreopool(gs=g.s.gs,param=g.s.param)([g.A,g.X])#reorder by last param
  graphs=gcomdiagraph(gs=g.s.gs,c=c)([graph])#diagonal graphs
  feats1=gcomparastract(gs=g.s.gs,param=g.s.param,c=c)([feat1])#abstract 3d parameters into 4d ones
  g.s.gs=int(g.s.gs/c)
  for i in range(multiglam):feats1=ganl(graphs,feats1,gs=c,a=g.s.gs,param=g.s.param,iterations=iterations,alin=alin)#run sub graph actions
  feat1=gcompoolmerge(gs=g.s.gs,ags=c,param=g.s.param,mode=pmode)([feats1])  
  graph=gcomgraphcutter(gs=g.s.gs*c,c=c,mode=gmode,cut=cut,c_const=c_const)([graph])#goes from big graph to small graph
  g.A=graph
  g.X=feat1
  if repeat>1:return gabstr(g,c,alin=alin,iterations=iterations,repeat=repeat-1,multiglam=multiglam,pmode=pmode,gmode=gmode)
  return g
def compress(g,c,addparam):
  g.X=gcompool(gs=g.s.gs,param=g.s.param,paramo=g.s.param+addparam,c=c,metrik_init=m.compression_init)([g.X])
  g.A=None
  g.s.gs=int(g.s.gs/c)
  g.s.param+=addparam
  return g




def graphatbottleneck(g,shallfp=True):#handles the bottleneck transformations for a pure graph ae, return g, compressed, new input, shallfp=True=>convert vector in matrix (gfromparam)
  comp=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  if m.shallredense:
    for e in m.redenseladder:
      comp=Dense(e,activation=m.redenseactivation,kernel_initializer=m.redenseinit)(comp)
    inn2=Input(m.redenseladder[-1])
    use=inn2
    for i in range(len(m.redenseladder)-1,-1,-1):
      use=Dense(m.redenseladder[i],activation=m.redenseactivation,kernel_initializer=m.redenseinit)(use)
    use=Dense(g.s.gs*g.s.param,activation=m.redenseactivation,kernel_initializer=m.redenseinit)(use)
  else:
    inn2=Input(g.s.gs*g.s.param)
    use=inn2

  if shallfp:
    taef1=gfromparam(gs=g.s.gs,param=g.s.param)([use])
  else:
    taef1=inn2
  g.X=taef1
  g.A=None
  return g,comp,inn2


def denseladder(c,n=3,truestart=False):
  if type(c)==list:
    ret=[]
    for ac in c:
      ret.append(denseladder(ac,n=n,truestart=truestart))
    return ret

  if truestart:n-=1

  #generates a list of Dense sizes going from 1 to c in n steps (excluding 1 and c)
  ret=[]
  if truestart:ret.append(1)
  fact=1.0
  mul=c**(1/(n+1))
  for i in range(n):
    fact*=mul
    ret.append(fact)
  return ret
def divtriv(g,c,shallgp=True,addDense=[[]],activation=None):#trivial graph diverger by a factor of c (does not chance param at all)
  #requiregp=True: require ggoparam at the start
  #addDense: intermediate Dense Layers, sizes between 1 and c useful



  param=g.X
  if shallgp:param=ggoparam(gs=g.s.gs,param=g.s.param)([param])
  for d in addDense[0]:param=Dense(int(d*g.s.gs*g.s.param),activation=activation,kernel_initializer=m.trivial_decompress_init_kernel,bias_initializer=m.trivial_decompress_init_bias)(param)
  g.s.gs*=c[0]#actually not completely the same as magic 29, since there was a param mistake
  param=Dense(g.s.gs*g.s.param,activation,kernel_initializer=m.trivial_decompress_init_kernel,bias_initializer=m.trivial_decompress_init_bias)(param)
  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([param])
  c=c[1:]
  if len(addDense)>0:
    addDense=addDense[1:]
  if len(c)>0:
    return divtriv(g,c,shallgp=False,addDense=addDense)
  return g
def divccll(g,c):
  g.X=gmultiply(gs=g.s.gs,param=g.s.param,c=c)([g.X])
  g.A=None
  g.s.gs*=c
  return g





def divpar(g,c,usei=False,alin=[],iterations=1,repeat=1,multiglam=1,amode2="prod"):#parameter like graph diverger by a factor of c (also does not chance param at all)

  #print("par on",g.A,g.X,g.s.gs,g.s.param,c)
  #exit()

  #amode2 : and operation modus for graphand2
  park0=gcomfullyconnected(gs=g.s.gs,param=g.s.param)([g.X])#generate initial fully connected graph
  taef1s=gcomdensediverge(gs=g.s.gs,param=g.s.param,paramo=g.s.param,c=c)([g.X])#list of param to list of list of param
  park1=gcomparamcombinations(gs=g.s.gs,param=g.s.param)([g.X])#all possible combinations of parameters
  park2=gcomgraphfrom2param(gs=g.s.gs,param=g.s.param,c=c)([park1])#make each combination into a graph
  park4=gcomgraphlevel(gs=g.s.gs,c=c)([park2])#make matrix of graphs into one graph
  park5=gcomgraphrepeat(gs=g.s.gs,c=c)([park0])#better:scale up park0 by replacing each 1/0 by a 1/0 c*c Matrix
  park6=gcomgraphand2(gs=g.s.gs,c=c,mode=amode2)([park4,park5])#merge the graphs (new graphs, with old graph)
  parkd=gcomextractdiag(gs=g.s.gs,c=c)([park2])#extract the needed matrices for glam from the combination generators

  #if usei:
  #  for i in range(multiglam):taef1s=gliam(gs=c,param=g.s.param,a=g.s.gs,alinearity=alin,iterations=iterations)([parkd,taef1s])#does the subnode graph actions
  #else: 
  #  for i in range(multiglam):taef1s=glam(gs=c,param=g.s.param,a=g.s.gs,alinearity=alin,iterations=iterations)([parkd,taef1s])#does the subnode graph actions
  for i in range(multiglam):taef1s=ganl(parkd,taef1s,gs=c,param=g.s.param,a=g.s.gs,alin=alin,iterations=iterations,usei=usei)

  taef2=gcomparamlevel(gs=g.s.gs,c=c,param=g.s.param)([taef1s])
  g.s.gs*=c
  g.X=taef2
  g.A=park6
  if repeat>1:return divpar(g,c,alin=alin,usei=usei,iterations=iterations,repeat=repeat,multiglam=multiglam,amode2=amode2)
  return g

def divcla(g,c,repeat=1):#classic graph abstractor, also does not chance the params, just goes from one param to c params, and has one learnable matrix (which is const)
  feat0,nmat=gcomdepoolplus(gs=g.s.gs,param=g.s.param,paramo=g.s.param,c=c,metrik_init=keras.initializers.Identity())([g.X])
  rmat=ggraphstract(in1=g.s.gs,in2=c)([g.A,nmat])
  g.A=rmat
  g.X=feat0
  g.s.gs*=c
  if repeat>1:return divcla(g,c,repeat=repeat-1)
  return g
def divcla2(g,c,repeat=1):#even more classic graph abstractor, just goes from one param to c params by Dense, does not chance the params
  feat0=gcomdepool(gs=g.s.gs,param=g.s.param,paramo=g.s.param,c=c,metrik_init=keras.initializers.Identity())([g.X])
  g.A=None
  g.X=feat0
  g.s.gs*=c
  if repeat>1:return divcla2(g,c,repeat=repeat-1)
  return g

def divgra(g,c,usei=False,alin=[],iterations=1,repeat=1,multiglam=1,amode="prod",amode2="prod"):#parameter like graph diverger by a factor of c (also does not chance param at all)
  #amode  : and operation modus for graphand
  #amode2 : and operation modus for graphand2
  park0=gcomfullyconnected(gs=g.s.gs,param=g.s.param)([g.X])#generate initial fully connected graph
  taef1s=gcomdensediverge(gs=g.s.gs,param=g.s.param,paramo=g.s.param,c=c)([g.X])#list of param to list of list of param  
  park1=gcomgraphfromparam(gs=g.s.gs,param=g.s.param,c=c)([g.X])#generates graphs from sets of params
  park2=gcomgraphcombinations(gs=g.s.gs,c=c)([park1])#build all combinations of these graphs
  park3=gcomgraphand(gs=g.s.gs,c=c,mode=amode)([park2])#combine graphs
  park4=gcomgraphlevel(gs=g.s.gs,c=c)([park3])#make matrix of graphs into one graph
  park5=gcomgraphrepeat(gs=g.s.gs,c=c)([park0])#better:scale up park0 by replacing each 1/0 by a 1/0 c*c Matrix
  park6=gcomgraphand2(gs=g.s.gs,c=c,mode=amode2)([park4,park5])#merge the graphs (new graphs, with old graph)

  #if usei:
  #  for i in range(multiglam):taef1s=gliam(gs=c,param=g.s.param,a=g.s.gs,alinearity=alin,iterations=iterations)([park1,taef1s])#does the subnode graph actions
  #else:
  for i in range(multiglam):taef1s=ganl(park1,taef1s,gs=c,param=g.s.param,a=g.s.gs,alin=alin,iterations=iterations,usei=usei)
  #  for i in range(multiglam):taef1s=glam(gs=c,param=g.s.param,a=g.s.gs,alinearity=alin,iterations=iterations)([park1,taef1s])#does the subnode graph actions
 
  taef2=gcomparamlevel(gs=g.s.gs,c=c,param=g.s.param)([taef1s])
  g.s.gs*=c
  g.X=taef2
  g.A=park6
  if repeat>1:return divgra(g,c,alin=alin,usei=usei,iterations=iterations,repeat=repeat,multiglam=multiglam,amode=amode,amode2=amode2)
  return g



def remparam(g,nparam):
  g.X=gremoveparam(gs=g.s.gs,inn=g.s.param,out=nparam)([g.X])
  g.s.param=nparam
  return g


def handlereturn(inn1,raw,com,inn2,decom,shallvae):
  #inn1  :   initial input Variable
  #raw   :   preconverted input Variable, for comparison sake
  #com   :   compressed Variable
  #inn2  :   input for decoder
  #decom :   decompressed decoder Variable
  #shallvae: shall i thread this like a variational auto encoder? hier just a bodge of an solution



  
  if not int(inn1.shape[-1])==4:
    print("returning failed at input1 shape",inn1.shape)
    assert False
  if not int(raw.shape[-1])==int(decom.shape[-1]):
    print("returning failed at comparison shape comparison(1) of",raw.shape,"and",decom.shape)
    assert False
  if len(raw.shape)>2:
    if not int(raw.shape[-2])==int(decom.shape[-2]):
      print("returning failed at comparison shape comparison(2) of",raw.shape,"and",decom.shape)
      assert False
  if not int(com.shape[-1])==int(inn2.shape[-1]):
    print("returning failed at compression shape comparison of",com.shape,"and",inn2.shape)
    assert False

  
  c1=com
  c2=com
  
  if shallvae:
    c1=Dense(int(c1.shape[-1]))(c1)
    c2=Dense(int(c2.shape[-1]))(c2)
    for i in range(100):print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("!!!running variational!!!")
    for i in range(100):print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
  return inn1,raw,c1,c2,[],inn2,decom

def sortparam(g,m):
  g.X=gssort(gs=g.s.gs,param=g.s.param,index=m.sortindex)([g.X])
  g.A=None
  return g 


def subedge(inp,param,m):
  feat1=Conv2D(param,(1,1),use_bias=m.edgeusebias)(inp)
  feat2=BatchNormalization()(feat1)
  feat3=Activation(m.edgeactivation)(feat2)
  return feat3

def edgeconv(inp,gs,k,param,m):
  feat0=dtcreate(gs=gs,k=k,param=param,flag=0)([inp])
  for i in range(m.edges):feat0=subedge(feat0,param,m=m)
  qfeat=dtdestroy(gs=gs,k=k,param=param)([feat0])
  outp=Activation(m.edgeactivationfinal)(qfeat)
  return outp

def ge(g,m,k=4):
  nw=edgeconv(g.X,g.s.gs,k=k,param=g.s.param,m=m)
  if m.edgeconcat:
    g.X=Concatenate(axis=-1)([g.X,nw])
    g.s.param*=2
  else:
    g.X=nw
  g.A=None
  return g


def shuffleinp(g,seed=None):#shuffles the param in a certain way, but keeps it const
  q=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  
  q=gshuffle(gs=1,param=g.s.param*g.s.gs,seed=seed)([q])

  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([q])

  return g

def orthoinp(g,seed=None):#shuffles the param in a certain way, but keeps it const
  q=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  
  q=gortho(gs=1,param=g.s.param*g.s.gs,seed=seed)([q])

  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([q])

  return g

def perminp(g):#shuffles the param in a better way, but keeps it const
  q=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  
  q=gperm(gs=1,param=g.s.param*g.s.gs)([q])

  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([q])

  return g

def pnorm(g):
  zw,g.X=gcutparam(gs=g.s.gs,param1=1,param2=g.s.param-1)([g.X])
  g.X=gpartinorm(gs=g.s.gs,param=g.s.param-1)([g.X])
  g.X=ghealparam(gs=g.s.gs,param1=1,param2=g.s.param-1)([zw,g.X])
  return g
  



def decompress(g,m,c):
  if type(c)!=list:c=[c]
  if m.decompress=="trivial":
    ###trivial magic29 decompression
    g,com,inn2=graphatbottleneck(g,shallfp=False)
    g=divtriv(g,c=c,shallgp=False,addDense=denseladder(c=c,n=m.trivial_ladder_n),activation=m.trivial_decompress_activation)
    return g,m,com,inn2
  if m.decompress=="paramlike":
    ###parameter like decompression
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divpar(g,c=ac,usei=m.usei)
      g=gnl(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="graphlike":
    ###graph like decompression
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divgra(g,c=ac,usei=m.usei)
      g=gnl(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="classic":
    ###classic decompression algorithm, with one learnable constant graph abstraction
    g,com,inn2=graphatbottleneck(g)
    g.A=gcomfullyconnected(gs=g.s.gs,param=g.s.param)([g.X])
    for ac in c:
      g=divcla(g,c=ac)
      g=gnl(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="classiclg":
    ###classic decompression algorithm, with learnable graph actions
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divcla2(g,c=ac)
      g=gll(g,usei=m.usei)
    return g,m,com,inn2
  if m.decompress=="ccll":
    ###copy copy learn learn decompression algorithm
    g,com,inn2=graphatbottleneck(g)
    for ac in c:
      g=divccll(g,c=ac)
      g=gll(g,usei=m.usei)
    return g,m,com,inn2


def createbothmodels(gs,out,n,shallvae=True,**kwargs):






  #not completely convinced that this actually does what i want (since main.py 0 1, also same spikes in other dataset)
  #missed some stuff (sort, norm), should be fixed

  assert gs==int(np.prod(spec))

  global m
  m=setting()
  m.usei=False
  m.decompress="trivial"

  m.trivial_ladder_n=1
  m.trivial_decompress_activation="linear"
  m.trivial_decompress_init_kernel=t.keras.initializers.TruncatedNormal()
  m.trivial_decompress_init_bias=t.keras.initializers.TruncatedNormal()

  m.sortindex=-1
  m.prenorm=False

  m.graph_init_self=t.keras.initializers.TruncatedNormal()
  m.graph_init_neig=t.keras.initializers.TruncatedNormal()
  m.agraph_init_self=m.graph_init_self
  m.agraph_init_neig=m.graph_init_neig

  m.edges=3#particle net like
  m.edgeactivation="relu"
  m.edgeactivationfinal=m.edgeactivation
  m.edgeusebias=False
  m.edgeconcat=False

  m.gq_activation="relu"
  m.gq_init_kernel=tf.keras.initializers.TruncatedNormal()
  m.gq_init_bias=tf.keras.initializers.Zeros()
  m.gq_usebias=False
  m.gq_batchnorm=False

  m.shallcomplex=not True
  m.complexsteps=3 

  m.gqa_activation=m.gq_activation
  m.gqa_init_kernel=m.gq_init_kernel
  m.gqa_init_bias=m.gq_init_bias
  m.gqa_usebias=m.gq_usebias
  m.gqa_batchnorm=m.gq_batchnorm

  m.shallacomplex=m.shallcomplex
  m.complexasteps=m.complexsteps

  m.shallredense=True
  m.redenseladder=[64,52,46,40,36]
  m.redenseactivation="relu"
  m.redenseinit=keras.initializers.Identity()

  m.compression_init=keras.initializers.Identity()


  c=6
  addparam=3


  s=state(gs=gs,param=None)
  g=grap(s)

  g,inn=prep(g,m)
  g=sortparam(g,m)
  #g=perminp(g)
  g=pnorm(g)



  g.X=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  g.s.param*=g.s.gs
  g.s.gs=1


  raw=g.X


  #g=gq(g,m)

  #g.X=glcreate(gs=g.s.gs,param=g.s.param)([g.A,g.X])
  #g.s.param*=2

  #print("gX",g.X.shape)

  #g.X=Dense(4)(g.X)

  #print("gX",g.X.shape)
  #exit()

  #g=ge(g,k=3,m=m)
  #g=learngraph(g,free=1)

  #g=abstr(g,c=c)





  ###variable decompression
  g,m,com,inn2=decompress(g,m,c=spec[::-1])



  g=remparam(g,nparam=4)

  #g=sortparam(g,m)

  g.X=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  g.s.param*=g.s.gs
  g.s.gs=1



  decom=g.X


  return handlereturn(inn,raw,com,inn2,decom,shallvae)












