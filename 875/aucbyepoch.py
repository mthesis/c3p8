import numpy as np
import matplotlib.pyplot as plt
import csv

from advload import *
from ageta import *
from adv2load import *


import sys

index=0
if len(sys.argv)>1:
  index=int(sys.argv[1])






f=np.load("..//..//toptagdataref//val_fairer.npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f

def makelenx(q,x):
  q=str(q)
  while len(q)<x:
    q="0"+q
  return q


with open("history.csv","r") as ff:
  c=csv.reader(ff,delimiter=",")
  jumped=False
  q=[]
  qn=[]
  epoch=[]
  for row in c:
    if not jumped:
      for e in row:
        q.append([])
        qn.append(e)
      jumped=True
      continue

    for i in range(len(row)):
      q[i].append(float(row[i]))


mp={}
for i in range(len(qn)):
  mp.update({qn[i]:q[i]})






def difference(a,b):
  #print(a.shape,b.shape,np.mean(a-b).shape)
  #exit()
  return np.sqrt(np.mean((a-b)**2))

def int(x,y):
  x,y=(list(t) for t in zip(*sorted(zip(x,y))))
  ret=0.0
  for i in range(1,len(x)):
    ret+=((y[i]+y[i-1])*(x[i]-x[i-1]))/2
  return ret


def calcauc(c,p,invert=False):
  
  
  d=np.zeros((len(y),))

  d0,d1=[],[]

  for i in range(len(y)):
    d[i]=difference(c[i],p[i])
    if (y[i]>0.5) != invert:
#    if np.random.randint(2)==0:
      d1.append(d[i])
    else:
      d0.append(d[i])


  cmax=np.max(d)
  cmin=np.min(d)
  cdel=(cmax-cmin)/1000
  if cdel==0:
    print("just 0 difference")
    return -1
  cs=np.arange(cmin,cmax,cdel)
  tpr,fpr,tnr,fnr=[],[],[],[]

  for c in cs:
    tp,fp,tn,fn=0,0,0,0
    for ad in d0:
      if ad>c:
        fp+=1#false positive, is zero, classified as one
      else:
        tn+=1#true negative, is zero, classified as zero
    for ad in d1:
      if ad>c:
        tp+=1#true positive, is one, classified as one
      else:
        fn+=1#false negative, is one, classified as zero
    tpr.append(tp/(tp+fn))
    fpr.append(fp/(fp+tn))
    tnr.append(tn/(tn+fp))
    fnr.append(fn/(fn+tp))



  auc= int(fpr,tpr)
  #if auc<0.5:auc=1-auc
  return auc






vl=mp["val_loss"]
l=mp["loss"]

vlosses=[]
losses=[]
aucs=[]

def save():
  np.savez_compressed("abe",l=losses,a=aucs,vl=vlosses)

def load():
  global index
  if index==0:return
  f=np.load("abe.npz")
  losses=list(f["l"])
  aucs=list(f["a"])
  vlosses=list(f["vl"])
  if index==-1:index=len(aucs)
  losses=losses[:index]
  aucs=aucs[:index]
  vlosses=vlosses[:index]

load()

for i in range(index,len(vl)):
  model=aload("models/weights"+makelenx(i+1,4)+".tf")
  comp=getcompbt(model)

  c=comp.predict(x)
  p=model.predict(x)
  losses.append(l[i])
  vlosses.append(vl[i])
  aucs.append(calcauc(c,p))
  save()

  for j in range(10):print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
  print("did",i,"of",len(vl))
  for j in range(10):print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

  



exit()




invert=True




f=np.load("evalb.npz")

x=f["x"]
y=f["y"]
p=f["p"]
c=f["c"]

#print(y)

#exit()









