import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from gpre5 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *
from gaddbias import *
from gssort import *
from winit import *
from glm import *
from glom import *#probably useless
from glim import *
from ggraphstract import *
from gmake1graph import *
from gcomdepoolplus import *
from gcomdex import *
from gcomjpool import *
from gcomgpool import *
from gchooseparam import *
from gcomdepoollg import *
from gcomdiagraph import *
from gcomreopool import *
from gcomparastract import *
from glam import *
from gcomdensemerge import *
from gcompoolmerge import *
from gcomgraphcutter import *
from gcomdensediverge import *
from gcomgraphfromparam import *
from gcomgraphcombinations import *
from gcomgraphand import *
from gcomgraphrepeat import *
from gcomgraphlevel import *
from gcomgraphand2 import *
from gcomparamlevel import *
from gliam import *
from gcomfullyconnected import *
from gcomparamcombinations import *
from gcomgraphfrom2param import *
from gcomextractdiag import *


objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk,"gltk":gltk,"gltknd":gltknd,"gpre1":gpre1,"gpre2":gpre2,"gpre3":gpre3,"gpre4":gpre4,"gpre5":gpre5,"glmlp":glmlp,"gltrivmlp":gltrivmlp,"gadd1":gadd1,"gaddzeros":gaddzeros,"gsym":gsym,"gbrokengrowth":gbrokengrowth,"gpoolgrowth":gpoolgrowth,"gremoveparam":gremoveparam,"gcompool":gcompool,"gcomdepool":gcomdepool,"gvaluation":gvaluation,"ggoparam":ggoparam,"gfromparam":gfromparam,"gaddbias":gaddbias,"gssort":gssort,"glm":glm,"glom":glom,"glim":glim,"ggraphstract":ggraphstract,"gmake1graph":gmake1graph,"gcomdepoolplus":gcomdepoolplus,"gcomdex":gcomdex,"gcomjpool":gcomjpool,"gcomgpool":gcomgpool,"gchooseparam":gchooseparam,"gcomdepoollg":gcomdepoollg,"gcomdiagraph":gcomdiagraph,"gcomreopool":gcomreopool,"gcomparastract":gcomparastract,"glam":glam,"gcomdensemerge":gcomdensemerge,"gcompoolmerge":gcompoolmerge,"gcomgraphcutter":gcomgraphcutter,"gcomdensediverge":gcomdensediverge,"gcomgraphfromparam":gcomgraphfromparam,"gcomgraphcombinations":gcomgraphcombinations,"gcomgraphand":gcomgraphand,"gcomgraphrepeat":gcomgraphrepeat,"gcomgraphlevel":gcomgraphlevel,"gcomgraphand2":gcomgraphand2,"gcomparamlevel":gcomparamlevel,"gliam":gliam,"gcomfullyconnected":gcomfullyconnected,"gcomparamcombinations":gcomparamcombinations,"gcomgraphfrom2param":gcomgraphfrom2param,"gcomextractdiag":gcomextractdiag}



  

k=4
param0=3
param1=4
param2=5

dim1=16##gs
dim2=8

k1=k
k2=k

alin=[-1.0,1.0]
it=5#why does that kinda work, but 10 wont
it=1
flag=0

constparam=1
complexityparam=0

usecompressionversion=1
usedecompressionversion=1


def somegraphactions(q,n,gs,param,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):#just a set of graph layers that should add complexity to the model without chaning dimensions in any way
  if n==0:return q
  mat1,feat1=gtopk(gs=gs,k=k,param=param,free=0,flag=flag,self_interaction=True)([q])
  #feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin)([mat1,feat1])
  #feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin,self_initializer=otherIdent,neig_initializer=keras.initializers.Zeros())([mat1,feat1])
  #feat2=glm(gs=gs,param=param,iterations=it,alinearity=alin,self_initializer=flipinit,neig_initializer=keras.initializers.Zeros())([mat1,feat1])
  feat2=glm(gs=gs,param=param,iterations=it,alinearity=alin,self_initializer=otherIdent,neig_initializer=keras.initializers.Zeros())([mat1,feat1])

  return somegraphactions(feat2,n=n-1,gs=gs,param=param,alin=alin,keepconst=keepconst,iterations=iterations,flag=flag,k=k)
def somegraphactionson(q,n,gs,param,mat,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):#just a set of graph layers that should add complexity to the model without chaning dimensions in any way
  if n==0:return q
  #mat1,feat1=gtopk(gs=gs,k=k,param=param,free=0,flag=flag,self_interaction=True)([q])
  feat1=q
  #feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin)([mat1,feat1])
  #feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin,self_initializer=otherIdent,neig_initializer=keras.initializers.Zeros())([mat1,feat1])
  #feat2=glm(gs=gs,param=param,iterations=it,alinearity=alin,self_initializer=flipinit,neig_initializer=keras.initializers.Zeros())([mat1,feat1])
  feat2=glm(gs=gs,param=param,iterations=it,alinearity=alin,self_initializer=otherIdent,neig_initializer=keras.initializers.Zeros())([mat,feat1])

  return somegraphactionson(feat2,mat=mat,n=n-1,gs=gs,param=param,alin=alin,keepconst=keepconst,iterations=iterations,flag=flag,k=k)
def someigraphactionson(q,n,gs,param,mat,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):#just a set of graph layers that should add complexity to the model without chaning dimensions in any way
  if n==0:return q
  #mat1,feat1=gtopk(gs=gs,k=k,param=param,free=0,flag=flag,self_interaction=True)([q])
  feat1=q
  #feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin)([mat1,feat1])
  #feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin,self_initializer=otherIdent,neig_initializer=keras.initializers.Zeros())([mat1,feat1])
  #feat2=glm(gs=gs,param=param,iterations=it,alinearity=alin,self_initializer=flipinit,neig_initializer=keras.initializers.Zeros())([mat1,feat1])
  feat2=glim(gs=gs,param=param,iterations=it,alinearity=alin,self_initializer=otherIdent,neig_initializer=keras.initializers.Zeros())([mat,feat1])

  return someigraphactionson(feat2,mat=mat,n=n-1,gs=gs,param=param,alin=alin,keepconst=keepconst,iterations=iterations,flag=flag,k=k)

def graphcompression(data,gs,param0,param1,c=2):
  feat0=gvaluation(gs=gs,param=param0)([data])
  feat1=gcompool(gs=gs,param=param0+1,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([feat0])
  return feat1
def graphcompressionv1(data,gs,param0,param1,c=2):
  #feat0=gvaluation(gs=gs,param=param0)([data])
  feat1=gcompool(gs=gs,param=param0,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([data])
  return feat1
def graphdecompression(data,gs,param0,param1,c=2):
  feat0=gcomdepool(gs=gs,param=param0,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([data])
  return feat0
def graphdecompressionv1(data,mat,gs,param0,param1,c=2):
  feat0,nmat=gcomdepoolplus(gs=gs,param=param0,paramo=param1,c=c,metrik_init=keras.initializers.Identity())([data])
  rmat=ggraphstract(in1=gs,in2=c)([mat,nmat])
  return feat0,rmat


def compress(data,s,np,c):
  if usecompressionversion==0:return graphcompression(data,s.gs,s.param,np,c),state(int(s.gs/c),np)
  if usecompressionversion==1:return graphcompressionv1(data,s.gs,s.param,np,c),state(int(s.gs/c),np)
def decompress(data,s,np,c,mat=None):
  if usedecompressionversion==0:return graphdecompression(data,s.gs,s.param,np,c),state(int(s.gs*c),np)
  if usedecompressionversion==1:
    ret1,ret2 = graphdecompressionv1(data,mat,s.gs,s.param,np,c)
    return ret1,ret2,state(int(s.gs*c),np)
def actions(data,s,n=1,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):
  return somegraphactions(data,n,s.gs,s.param,alin,keepconst,iterations,flag,k),s
def actionson(data,s,mat,n=1,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):
  return somegraphactionson(data,n,s.gs,s.param,mat,alin,keepconst,iterations,flag,k),s
def iactionson(data,s,mat,n=1,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):
  return someigraphactionson(data,n,s.gs,s.param,mat,alin,keepconst,iterations,flag,k),s
#def multidense(data,d,activation="relu",kernel_initializer=keras.initializers.TruncatedNormal()):#if applied to 2d data(nodes, attributes), works on each node
def multidense(data,d,activation="relu",kernel_initializer=keras.initializers.Identity()):#if applied to 2d data(nodes, attributes), works on each node
  if len(d)==0 or False:return data
  ac=d[0]
  d=d[1:]

  lay=Dense(int(ac),activation=activation,kernel_initializer=kernel_initializer)
  data=lay(data)
  return multidense(data,d,activation=activation,kernel_initializer=kernel_initializer)


def prep(g):#preparation handler, return g and input
  inp=Input(shape=(g.s.gs,4))
  feat0=gpre5(gs=g.s.gs)(inp)
  g.X=feat0
  g.s.param=4
  return g,inp

flag=0
self_interaction=True

def gnl(g,alin=[],iterations=1,repeat=1):#simple graph layer system, without learnable graphs
  
  g.X=glm(gs=g.s.gs,param=g.s.param,iterations=iterations,alinearity=alin)([g.A,g.X])
  if repeat>1:return gnl(g,alin=alin,iterations=iterations,repeat=repeat-1)
  return g

def gll(g,free=0,alin=[],iterations=1,repeat=1,subrepeat=1):#simple graph layer system, with learnable graphs
  mat,feat=gtopk(gs=g.s.gs,param=g.s.param,free=free,flag=flag,self_interaction=self_interaction)([g.X])
  g.A=mat
  g.s.param+=free
  g.X=feat
  g=gnl(g,alin=alin,iterations=iterations,repeat=subrepeat)
  if repeat>1:return gll(g,free=free,alin=alin,iterations=iterations,repeat=repeat-1)
  return g

cut=0.5
c_const=1000.0

def abstr(g,c,alin=[],iterations=1,repeat=1,multiglam=1,pmode="max",gmode="mean"):#uses (multiglam) glam to abstract a graph into a factor c smaller graph
  #uses pooling to go from c size subgraphs to 1 size dots
  #does not chance param at all
  #uses (pmode) param pooling mode
  #uses (gmode) graph pooling mode
  graph,feat1=gcomreopool(gs=g.s.gs,param=g.s.param)([g.A,g.X])#reorder by last param
  graphs=gcomdiagraph(gs=g.s.gs,c=c)([graph])#diagonal graphs
  feats1=gcomparastract(gs=g.s.gs,param=g.s.param,c=c)([feat1])#abstract 3d parameters into 4d ones
  g.s.gs=int(g.s.gs/c)
  for i in range(multiglam):feats1=glam(gs=c,a=g.s.gs,param=g.s.param,iterations=iterations,alinearity=alin)([graphs,feats1])#run sub graph actions
  feat1=gcompoolmerge(gs=g.s.gs,ags=c,param=g.s.param,mode=pmode)([feats1])  
  graph=gcomgraphcutter(gs=g.s.gs*c,c=c,mode=gmode,cut=cut,c_const=c_const)([graph])#goes from big graph to small graph
  g.A=graph
  g.X=feat1
  if repeat>1:return gabstr(g,c,alin=alin,iterations=iterations,repeat=repeat-1,multiglam=multiglam,pmode=pmode,gmode=gmode)
  return g

def graphatbottleneck(g,shallfp=True):#handles the bottleneck transformations for a pure graph ae, return g, compressed, new input, shallfp=True=>convert vector in matrix (gfromparam)
  comp=ggoparam(gs=g.s.gs,param=g.s.param)([g.X])
  inn2=Input(g.s.gs*g.s.param)
  if shallfp:
    taef1=gfromparam(gs=g.s.gs,param=g.s.param)([inn2])
  else:
    taef1=inn2
  g.X=taef1
  g.A=None
  return g,comp,inn2

def denseladder(c,n=3):
  #generates a list of Dense sizes going from 1 to c in n steps (excluding 1 and c)
  ret=[]
  fact=1.0
  mul=c**(1/(n+1))
  for i in range(n):
    fact*=mul
    ret.append(fact)
  return ret

def divtriv(g,c,shallgp=True,addDense=[]):#trivial graph diverger by a factor of c (does not chance param at all)
  #requiregp=True: require ggoparam at the start
  #addDense: intermediate Dense Layers, sizes between 1 and c useful
  param=g.X
  if shallgp:param=ggoparam(gs=g.s.gs,param=g.s.param)([param])
  for d in addDense:param=Dense(int(d*g.s.gs*g.s.param))(param)
  g.s.gs*=c#actually not completely the same as magic 29, since there was a param mistake
  param=Dense(g.s.gs*g.s.param)(param)
  g.X=gfromparam(gs=g.s.gs,param=g.s.param)([param])
  return g

def remparam(g,nparam):
  g.X=gremoveparam(gs=g.s.gs,inn=g.s.param,out=nparam)([g.X])
  g.s.param=nparam
  return g

def handlereturn(inn1,raw,com,inn2,decom):
  #inn1  :   initial input Variable
  #raw   :   preconverted input Variable, for comparison sake
  #com   :   compressed Variable
  #inn2  :   input for decoder
  #decom :   decompressed decoder Variable
  
  if not int(inn1.shape[-1])==4:
    print("returning failed at input1 shape",inn1.shape)
    assert False
  if not int(raw.shape[-1])==int(decom.shape[-1]):
    print("returning failed at comparison shape comparison(1) of",raw.shape,"and",decom.shape)
    assert False
  if len(raw.shape)>2:
    if not int(raw.shape[-2])==int(decom.shape[-2]):
      print("returning failed at comparison shape comparison(2) of",raw.shape,"and",decom.shape)
      assert False
  if not int(com.shape[-1])==int(inn2.shape[-1]):
    print("returning failed at compression shape comparison of",com.shape,"and",inn2.shape)
    assert False

  


  return inn1,raw,com,com,[],inn2,decom


class state:
  gs=10
  param=10
 

  def __init__(self,gs,param):
    self.gs=gs
    self.param=param
  def __repr__(self):
    return str(self.gs)+"*"+str(self.param)


class grap:
  A=None
  X=None
  s=None

  def __init__(self,s):
    self.A=None
    self.X=None
    self.s=s

def createnew():
  s=state(gs=6,param=None)
  g=grap(s)

  g,inn=prep(g)

  raw=g.X

  c=3


  g=gll(g,free=1)
  g=abstr(g,c=c)
  g=gnl(g)
  g,com,inn2=graphatbottleneck(g,shallfp=False)

  ladder=[]
  ladder=denseladder(c=c,n=4)

  g=divtriv(g,c=c,shallgp=False,addDense=ladder) 
  g=remparam(g,nparam=4)

  decom=g.X


  return handlereturn(inn,raw,com,inn2,decom)




def createminimal():
  gs=6#always keep gs=6

  inputs=Input(shape=(gs,4))

  feat0=gpre5(gs=gs)(inputs)

  #feat0=gssort(gs=gs,param=params[0],index=sortindex)([feat0])#assume already sortet...as is actually always a good idea

  s=state(gs=gs,param=int(feat0.shape[-1]))


  #print("feat0",feat0.shape)
 
  feat1=feat0

  graph,feat1=gtopk(gs=s.gs,param=s.param,free=1,flag=flag,self_interaction=True)([feat1])
  #print("graph",graph.shape,"feat1",feat1.shape)
  s.param+=1
  feat1=glm(gs=s.gs,param=s.param,iterations=1,alinearity=[])([graph,feat1])
  #print("feat1",feat1.shape)

  graph,feat1=gcomreopool(gs=s.gs,param=s.param)([graph,feat1])
  #print("graph",graph.shape,"feat1",feat1.shape)

  c1=3
  graphs=gcomdiagraph(gs=s.gs,c=c1)([graph])
  #print("graphs",graphs.shape)

  feats1=gcomparastract(gs=s.gs,param=s.param,c=c1)([feat1])
  s.gs=int(s.gs/c1)
  #print("feats1",feats1.shape)  

  feats1=glam(gs=c1,a=s.gs,param=s.param,iterations=1,alinearity=[])([graphs,feats1])

  #deltaparam=2
  feat1=gcompoolmerge(gs=s.gs,ags=c1,param=s.param)([feats1])
  #s.param+=deltaparam

  graph=gcomgraphcutter(gs=s.gs*c1,c=c1)([graph])

  feat1=glm(gs=s.gs,param=s.param,iterations=1,alinearity=[])([graph,feat1])



  #print("feat1",feat1.shape)

  #exit() 

  compressed=ggoparam(gs=s.gs,param=s.param)([feat1])



  inn2=Input(s.gs*s.param)


  taef1=gfromparam(gs=s.gs,param=s.param)([inn2])
  print("taef1",taef1.shape)


  park0=gcomfullyconnected(gs=s.gs,param=s.param)([taef1])
  print("park0",park0.shape)


  taef1s=gcomdensediverge(gs=s.gs,param=s.param,paramo=s.param,c=c1)([taef1])
  print("taef1s",taef1s.shape)


  park1=gcomparamcombinations(gs=s.gs,param=s.param)([taef1])
  print("park1",park1.shape)



  park2=gcomgraphfrom2param(gs=s.gs,param=s.param,c=c1)([park1])
  print("park2",park2.shape)
 

  #exit()


  park4=gcomgraphlevel(gs=s.gs,c=c1)([park2])
  print("park4",park4.shape)

  park5=gcomgraphrepeat(gs=s.gs,c=c1)([park0])


  print("park5",park5.shape)
  #exit()

  park6=gcomgraphand2(gs=s.gs,c=c1)([park4,park5])  


  #print("park1",park1.shape,"taef1s",taef1s.shape)
  #exit()

  parkd=gcomextractdiag(gs=s.gs,c=c1)([park2])


  taef2s=glam(gs=c1,param=s.param,a=s.gs,alinearity=[],iterations=1)([parkd,taef1s])

  print("taef2s",taef2s.shape,s)

  taef2=gcomparamlevel(gs=s.gs,c=c1,param=s.param)([taef2s])

  print("taef2",taef2.shape,s)
  s.gs*=c1
  print(s)

  #exit()


  taef3=glm(gs=s.gs,param=s.param,iterations=1,alinearity=[])([park6,taef2])

  print("taef3",taef3.shape,s)

  taef4=gremoveparam(gs=s.gs,inn=s.param,out=s.param-1)([taef3])
  s.param-=1

  decompressed=taef4
  


  #####graph like decompression
  #  inn2=Input(s.gs*s.param)
  #
  #
  #  taef1=gfromparam(gs=s.gs,param=s.param)([inn2])
  #
  #  park0=gcomfullyconnected(gs=s.gs,param=s.param)([taef1])
  #  
  #
  #
  #  taef1s=gcomdensediverge(gs=s.gs,param=s.param,paramo=s.param,c=c1)([taef1])
  #
  #
  #
  #  park1=gcomgraphfromparam(gs=s.gs,param=s.param,c=c1)([taef1])
  #
  #  park2=gcomgraphcombinations(gs=s.gs,c=c1)([park1])
  #
  #  park3=gcomgraphand(gs=s.gs,c=c1)([park2])
  #
  #  #print("park2",park2.shape,"park3",park3.shape)
  #  #exit()
  #
  #  park4=gcomgraphlevel(gs=s.gs,c=c1)([park3])
  #  print("park4",park4.shape)
  #
  #  park5=gcomgraphrepeat(gs=s.gs,c=c1)([park0])
  #
  #
  #  print("park5",park5.shape)
  #  #exit()
  #
  #  park6=gcomgraphand2(gs=s.gs,c=c1)([park4,park5])  
  #
  #
  #  #print("park1",park1.shape,"taef1s",taef1s.shape)
  #  #exit()
  #
  #  taef2s=glam(gs=c1,param=s.param,a=s.gs,alinearity=[],iterations=1)([park1,taef1s])
  #
  #  print("taef2s",taef2s.shape,s)
  #
  #  taef2=gcomparamlevel(gs=s.gs,c=c1,param=s.param)([taef2s])
  #
  #  print("taef2",taef2.shape,s)
  #  s.gs*=c1
  #  print(s)
  #
  #  #exit()
  #
  #
  #  taef3=glm(gs=s.gs,param=s.param,iterations=1,alinearity=[])([park6,taef2])
  #
  #  print("taef3",taef3.shape,s)
  #
  #  taef4=gremoveparam(gs=s.gs,inn=s.param,out=s.param-1)([taef3])
  #  s.param-=1
  #
  #  decompressed=taef4
  


  #trivial decompression
  #  s.gs=gs
  #  s.param=4
  #  subcompressed=Dense(s.gs*s.param)(inn2)
  #  decompressed=gfromparam(gs=s.gs,param=s.param)([subcompressed])


  #print("feat0",feat0.shape,"decompressed",decompressed.shape)
  #exit()

  return inputs,feat0,compressed,compressed,[],inn2,decompressed




def createbothmodels(gs,out,n,shallvae=True,**kwargs):
  ##trivial way
#  inputs,do1,do2=createmodel(gs,out,n,**kwargs)
#  output=createantimodel(gs,out,n,**kwargs)
#  return inputs,do1,do2,output
  


  assert gs==6
  return createnew()

  return createminimal()


  assert gs==16




  alin=[-1.0,1.0]
  alin=[]


  ns=       [1,1]#how many different graph actions
  its=      [1,1]#repeat them how often
  ks=       [6,2]#how many nearest neighbours
  cs=       [4,4]#compressing by factor
  #gs=       4                (before)
  #params=[4 ,14 ,50 ]
  params=[4 ,10,24]#reaching a parameter size of
  #params=[4,16,64]
  
  #params=[3 ,10 ,30 ,60]#for some reason nans




  #ns=list(np.zeros_like(ns))



  selfnodeact=np.array([1.5,2.0,1.0])#multiplied with current param, 1.0 at end needed
  selfnodeact=np.array([1.0,1.0,1.0])#multiplied with current param, 1.0 at end needed
  selfnodeact=np.array([1.0])

  #needs to start with the input dimension, and end with the output dimension
  #(both get deleted in their specific cases)
  #comp=[params[-1],40,30]
  comp=[params[-1],18,12]#compressing this parameters further
  #comp=[params[-1],params[-1],params[-1]]
  #comp=[params[-1],20,10,5]
  #comp=[params[-1],params[-1]]

  sortindex=-1
 


  ins=ns.copy()
  ins.reverse()
  iits=its.copy()
  iits.reverse()
  iks=ks.copy()
  iks.reverse()
  ics=cs.copy()
  ics.reverse()
  iparams=params.copy()
  iparams.reverse()
  iparams.pop(0)

  decomp=comp.copy()
  decomp.reverse()
  comp.pop(0)
  decomp.pop(0)


  ##encoder
  inputs=Input(shape=(gs,4,))
  feat0=gpre5(gs=gs)(inputs)

  feat0=gssort(gs=gs,param=params[0],index=sortindex)([feat0])

  s=state(gs=gs,param=params.pop(0))

  #feat0=ggoparam(gs=s.gs,param=s.param)([feat0])
  feat1=BatchNormalization(axis=-1,scale=True)(feat0)
  #feat0=gfromparam(gs=s.gs,param=s.param)([feat0])

  #feat1=feat0


  while len(params)>0:
    feat1,s=actions(feat1,s,n=ns.pop(0),alin=alin,iterations=its.pop(0),k=ks.pop(0))#first actions, needed to go to gs=1 and for compression1

    #params.pop(0)

    feat1=multidense(feat1,selfnodeact*s.param)

    feat1,s=compress(feat1,s,params.pop(0),cs.pop(0))


  feat1=ggoparam(gs=1,param=s.param)([feat1])
  
  
  cdim=comp[-1]

  do0=multidense(feat1,comp.copy())
  doa=multidense(do0,[cdim])
  if shallvae:
    dob=multidense(do0,[cdim])
  else:
    dob=doa
  
  #doa=feat1  
  #cdim=int(doa.shape[-1])


  mats=[]

  ##decoder
  inputs2=Input(shape=(cdim))
 
  taef0=multidense(inputs2,decomp)
  taef0=multidense(taef0,[s.gs*s.param])
  #print(taef0.shape)
  #print(s.gs,s.param)
  #exit()

  taef0=gfromparam(gs=s.gs,param=s.param)([taef0])

  #print(taef0.shape)
  #exit()


  grap=gmake1graph()([taef0])


  taef1=taef0

  while len(iparams)>0:
    taef1,grap,s=decompress(taef1,s,iparams.pop(0),ics.pop(0),mat=grap)
    taef1,s=iactionson(taef1,s=s,n=ins.pop(0),alin=alin,iterations=iits.pop(0),k=iks.pop(0),mat=grap)

  #taef1=gaddbias(gs=s.gs,param=s.param)([taef1])
  taef1=gssort(gs=s.gs,param=s.param,index=sortindex)([taef1])

  inn=[inputs2]

  #model=Model(inputs=inputs2,outputs=taef14,name="decoder")
  #returns input layer (this which we want to compare the the output), mean, logvar,decoder
  #return inputs,doa,dob,model
 

  #print("shall compare",feat0.shape,taef1.shape)
  #exit()

  return inputs,feat0,doa,dob,mats,inn,taef1
  #has to return:
    #input of encoder
    #comparison object
    #mean, logvar of encoder output
    #list of ajacency matrices
    #input of decoder
    #output of decoder

















