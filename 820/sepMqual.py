import numpy as np
import matplotlib.pyplot as plt
import os
import json

from cauc import caucd
from trainingon import *

import sys

goaly=None
if len(sys.argv)>1:
  goaly=[int(sys.argv[1])]
  for ss in sys.argv[2:]:
    goaly.append(int(ss))




def caucdp(d,y):
  ret=caucd(d=d,y=y)
  if ret["auc"]<0.5:
    rel=caucd(d=d,y=1-y)
    ret["e30"]=rel["e30"]
    ret["i30"]=rel["i30"]
  return ret

def getc():
  return np.sum([1 for q in os.listdir(".") if "sMeval" in q and ".npz" in q])
def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}
fnams=list([f"sMeval{id}.npz" for id in [1,2,3,4,5]])
fnams=list([f"sMeval{id}.npz" for id in range(1,7+1)])
fnams=list([f"sMeval{id}.npz" for id in range(0,getc())])


def trainonwhich(trainingon):
  if "1" in trainingon:return 1
  if "0" in trainingon:return 0
  return -1

t=trainonwhich(trainingon)

def deltamean(s,y,t):
  d=np.mean(s,axis=-1)
  if t<0.5:
    dm=np.mean(d[np.where(y<0.5)])
  else:
    dm=np.mean(d[np.where(y>0.5)])
  ret=np.abs(d-dm)
  ret/=np.std(ret)
  return ret

fs=list([np.load(q) for q in fnams])

if not goaly is None:
  jobs=[f["job"] for f in fs]
  jobl=[len(job) for job in jobs]

  if -1 in goaly:
    plt.hist(jobl)
    plt.show()

  fs=[f for f,l in zip(fs,jobl) if l in goaly]





ss=list([f["s"] for f in fs])
y=fs[0]["y"]

ds=list([deltamean(s,y,t) for s in ss if np.std(s)>0.0001])


qs=list([caucdp(d=d,y=y) for d in ds])

aucs=list([q["auc"] for q in qs])
e30s=list([q["e30"] for q in qs])

print("simple aucs:",*aucs)
print("simple e30s:",*e30s)

print("aucs_min",np.min(aucs))

sumd=np.sum(np.array(ds),axis=0)
sq=caucdp(d=sumd,y=y)
sauc=sq["auc"]
se30=sq["e30"]

print("sum auc:",sauc)
print("sum e30:",se30)

print("correlation")
if len(qs)<10:
  print(np.corrcoef(ds))
else:
  print(json.dumps(statinf(np.corrcoef(ds)),indent=2))

np.savez_compressed("sepMout",auc=sauc,e30=se30,n=len(qs),aucs=aucs,e30s=e30s)



